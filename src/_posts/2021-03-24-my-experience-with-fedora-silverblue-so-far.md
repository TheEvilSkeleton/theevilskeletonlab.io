---
layout: post
title: "My Experience with Fedora Silverblue so far"
description: "Fedora Silverblue is an immutable Linux distribution, meaning the state of the operating system (OS) cannot be tampered at all. The OS is read-only, meaning the contents cannot be written by any user, including root. Some directories, such as `/etc` are read-write however. Each time a change is occured, the distribution will generate a tree (bootable image) in which you can boot from once the tree has been completed.


This page is about my experience with Fedora Silverblue, what I dislike about it and what I like about it. A slight warning that this page is opinionated."
toc: true
tags: ["Fedora", "Flatpak"]
redirect_from: /2021/03/24/my-experience-with-fedora-silverblue-so-far.html
---

## Introduction

{{ page.description }}

## What I dislike about it
Okay, since I'm the type of person that likes to save the best for last, I am going to start with what I dislike in Fedora Silverblue.

### ~~Reboots~~
~~It requires a reboot for changes to take effect everytime a package is upgraded, installed or even removed. Since Fedora Silverblue is immutable, it needs to create a new tree with the changes made so the user can then boot from. Fortunately Flatpak mitigates this issue, but it is still an annoyance when you want to install a basic program, like fish, but you are forced to reboot for the change to take effect.~~ This is false! Running `rpm-ostree ex apply-live` solves this issue. Credit to [LionirDeadman](https://lionir.ca/).

### Learning Curve
The learning curve required with the lack of proper documentation is also something I dislike a lot. Sure, everything requires a learning curve, but with Fedora Silverblue, it requires a steeper learning curve. I don't think this is a major flaw, but pair it with the lack of proper documentation, the learning curve can become far more steeper than it should. Documentation is often half-assed, and Fedora Silverblue's documentation is no exception. The tools Fedora Silverblue recommends: Toolbox and Podman, also lack proper documentation, which only makes the experience in Fedora Silverblue annoying.

### Fedora Flatpak Remote
I am personally not fond of Fedora's Flatpak remote, despite the remote being [OCI](https://en.wikipedia.org/wiki/Open_Container_Initiative) compliant which I am a really big fan of, I don't think Fedora's Flatpak remote should be used by default, or even at all. I've had many issues with packages in that remote, such as using X11's cursor instead of Adawaita's and sometimes not even launching. Flathub would be a better option.

## What I like about it

I like its concept and how it's implemented. Fedora Silverblue uses a technology called OSTree, which is also known as "Git for binaries". It is used to track binary files, and uses similar terminologies to Git, such as commit, refs and branches, remotes, etc. Thanks to this, I find it very familiar to Git, even though I haven't used OSTree for a long time.

### rpm-ostree
rpm-ostree is a package manager of Fedora Silverblue. Just like DNF, the package manager of Fedora Workstation, rpm-ostree lets you install RPM packages on the system. It is essentially the "DNF of OSTree". Using the normal Fedora repositories, I have access to all the Fedora Workstation packages without needing to implement obscure workarounds; I can also add external repositories like RPM Fusion, repositories from [Copr](https://copr.fedorainfracloud.org/) (the PPA of Fedora), and more.

### Flatpak
Fedora Silverblue embraces Flatpak. I personally adore Flatpak because of its Linux (including non-GNU) distribution agnostism, consistent builds, easy permission overrides, and of course, the use of OSTree. Flatpak improves exponentially the experience in Fedora Silverblue, as it doesn't need the system to be rebooted every time you install a package. Most of the GUI applications I use are available in Flathub, so I hardly need to install anything using rpm-ostree. The sandboxes are very useful as well, since we live in an era where sandboxing is crucial for the end user's system so it can be used daily to mitigate bugs and to prevent vulnerabilities from spreading, which is something that was always lacking on Linux.

### Uninterruption
Updates cannot be interrupted. Most Linux distributions have a process of installing, removing, upgrading and sometimes even downgrading packages. If the process of the aformentioned gets interrupted by e.g. crash, or accidentally killing the process midway, it can be fatal to your system. Fedora Silverblue is made to completely solve that issue by generating bootable trees when a change has been done.

### Upgrading & Downgrading
Upgrading and downgrading are incredibly easy. Upgrading is as simple as running `rpm-ostree upgrade`, similar to any other package manager. The main benefit in Fedora Silverblue is that when the new tree is finished, the distribution creates a new snapshot of the older version, which can be used to easily downgrade if something goes wrong. Just a simple `rpm-ostree rollback` and your system goes back to what it was before upgrading. You can also use `rpm-ostree rebase` to rebase onto the desired ref (branch). Let's say you want to try Fedora Silverblue Rawhide (bleeding edge Fedora Silverblue), you can run the following to set the remotes up:

```terminal
$ ostree remote refs fedora | grep "x86_64"
[...]
fedora:fedora/33/x86_64/silverblue
fedora:fedora/33/x86_64/testing/silverblue
fedora:fedora/33/x86_64/updates/silverblue
fedora:fedora/34/x86_64/silverblue
fedora:fedora/34/x86_64/testing/silverblue
fedora:fedora/rawhide/x86_64/atomic-host
fedora:fedora/rawhide/x86_64/silverblue
fedora:fedora/rawhide/x86_64/workstation
fedora:fedora/x86_64/coreos/next
fedora:fedora/x86_64/coreos/stable
fedora:fedora/x86_64/coreos/testing

$ rpm-ostree rebase fedora:fedora/rawhide/x86_64/silverblue
[...]
```

Reboot, and that's it! You get to use the latest and greatest packages in Fedora Silverblue. If you are not satisfied with Rawhide, all you can do is run `rpm-ostree rollback`, then reboot and you are back to your previous setup.

### Containers
After using Fedora Silverblue for a bit, I started to get interested with containers. Containers are fun to play with, and the amount of benefit one can get is a lot. Podman, a Docker drop-in replacement, Toolbox and systemd-nspawn are all very fun to play with. Toolbox doesn't require root, which is very beneficial on a security standpoint. I set up Jekyll inside a [Debian container](https://framagit.org/TheEvilSkeleton/debian-jekyll), and aliased `$COMMAND` with `toolbox run -c debian-jekyll $COMMAND` to make it look like the commands are running on the host system.

**Addendum – Aug 8, 2021**

I have recently deprecated Debian Jekyll in favor to [Jekyll Toolbox](https://gitlab.com/TheEvilSkeleton/jekyll-toolbox). It is smaller in size, doesn't contain anything from Debian and now comes with its own [images](https://gitlab.com/TheEvilSkeleton/jekyll-toolbox/container_registry). More information on my [blog post](../../07/10/linux-jekyll-toolbox-container.html).

### Per-User installation
rpm-ostree allows the user to install packages only in their home directory. That is very useful for someone like me, as I want my root partition to be as small as possible, to keep my `/home` partition as big as possible for user data. By using traditional package management system, I am forced to install everything on my root partition, which can take megabytes or even gigabytes of storage space on my root partition, which is not what I want. Having it installed in my home directory is very beneficial for space and for portability, since I can just reinstall Fedora Silverblue and reuse my current `/home` partition in case I somehow manage to brick it, and still have my data along with my applications installed.

### btrfs
btrfs became very stable since Linux 5.0, and became the best ext4 drop-in replacement since then. In Fedora 33, they have switched to btrfs by default, which is a really big jump in the Linux desktop, as they were using ext4 since the beginning. openSUSE had switched to btrfs a long time ago and have been contributing to it for a very long time. Fedora followed up a couple of years later by switching to btrfs by default. Fedora Silverblue is no exception, as it got the same treatment as well. btrfs has many optimizations available for SSDs, compared to ext4. It also supports RAID and other modern features.

## Conclusion
I haven't played around with Fedora Silverblue for so long. I have been using it for a month or two, but I have started playing around with it for a couple of weeks as of time of writing this post. I am fond of Fedora Silverblue, and I love the direction it is heading. I've started to get interested with containers and found really fun to play around with them, and got to learn to create Dockerfiles, create containers, etc.

The drawbacks I've mentioned earlier are mostly annoyances for me, and not dealbreakers. I don't install many software on my system, so I don't have to reboot very often. With trial and error, I can start learning more about containers, and I can simply remove the Fedora Flatpak remote in favor to Flathub.

Prior to Fedora Silverblue, I used to use Fedora Workstation. I stopped liking Fedora Workstation due to the traditional package management. Before Fedora Workstation, I used NixOS, and I liked it for the same reason I like Fedora Silverblue, but I switched from it due to not understanding immutable desktops. As of today, Fedora Silverblue, NixOS and openSUSE are my favorite distributions. The maintainers, developers and contributors of said distributions are innovative in their own way; they come with brilliant ideas and implement them correctly. I want to thank each and every one of the maintainers, developers and contributors that have made those distributions great and innovative.
