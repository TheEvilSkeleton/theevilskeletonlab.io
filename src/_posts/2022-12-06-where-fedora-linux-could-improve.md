---
layout: post
title: "Where Fedora Linux Could Improve"
description: "The Fedora Project is a great organization to gain experience no matter the team you are in. I am currently a part of the Fedora Websites & Apps team improving my technical writing, communication and design skills.<br><br>With all the things the Fedora Project does well, there are several places that, in my opinion, need to be improved. I'd like to go over some key areas where we could improve Fedora Linux from a user perspective without breaking the Fedora Project's core philosophies."
tags: ["Fedora", "Linux"]
toc: true
disclaimer: "
1. I am not employed by Red Hat (yes, I have to say that).

1. I may sound rude, so I sincerely apologize for that, as I am really critical in some of these topics.

1. \"Fedora Project\" is referred to the organization. \"Fedora Linux\" is referred to Fedora operating systems, like Fedora Workstation, Fedora KDE Plasma Desktop Edition, Fedora Silverblue, and others. \"Fedora Linux community\" is referred to the Fedora Project/Linux community, such as developers, contributors and users."
redirect_from: /2022/12/06/where-fedora-linux-could-improve.html
---

## Introduction

{{ page.description }}

## Unification and Uniformity
In my opinion, Fedora Linux suffers from severe inconsistencies, such as website design and distribution naming scheme, which can be quite daunting to learn from an end user perspective.

### Standard Styles and Layouts
At the moment, many Fedora Linux pages look inconsistent from one another. [Fedora Workstation], [Fedora Server] and [Fedora IoT] follow a common style and layout, whereas [Fedora Silverblue], [Fedora Kinoite] and [Fedora CoreOS] don't. This means there are a total of four entirely different styles and layouts. This gives the impression that Fedora Silverblue, Fedora Kinoite and Fedora CoreOS are external projects and are barely related to the Fedora Project, rather than official projects endorsed by the Fedora Project.

I believe that we should focus on standardizing by unifying the styles and layouts in a single framework, instead of reinventing the wheel with no standardization in mind. Luckily, [Fedora Kinoite and Fedora CoreOS website maintainers](https://github.com/fedora-silverblue/silverblue-site/issues/114#issuecomment-1262520819) are in favor of unifying; hopefully everyone else is on board and we can provide a platform for internal projects.

### Naming Convention
Similar to Fedora Linux pages, I believe Fedora Linux lacks a naming convention. For example, Fedora Workstation uses GNOME and uses the traditional Linux desktop paradigm for managing packages. On the other hand, Fedora Silverblue is visually the same as Fedora Workstation, with the main difference that it reinvents the Linux desktop paradigm to solve fundamental issues with the Linux desktop. The name "Fedora Workstation" conveys what it is intended for, whereas "Fedora Silverblue" doesn't. In reality, "Fedora Silverblue" doesn't have a clear meaning, and doesn't even give a hint to users about what it is trying to do or be.

This lack of a naming convention expands with [Fedora KDE Plasma Desktop Edition] (or Fedora KDE for short) and Fedora Kinoite for Plasma, Fedora Server and CoreOS for server, and others.

Furthermore, with Fedora Silverblue/Kinoite/CoreOS, there is no concept of spins and editions. Despite being fundamentally the same, each is treated as an entirely separate project. For example, Fedora KDE is Fedora Workstation with KDE Plasma instead of GNOME, whereas on the immutable end, Fedora Kinoite is Fedora Silverblue with KDE Plasma. Despite this equivalency, Fedora KDE is a spin, whereas Fedora Kinoite is a whole separate distribution, when it could be a spin instead.

To address this, a category that englobes all immutable distributions would not only improve the naming scheme, it would also be easier for users to reference all immutable distributions without needing to enumerate through them. For example, instead of Fedora Silverblue, it could be Fedora Immutable Workstation, or Fedora Atomic Workstation. For Plasma, it could be Fedora Immutable KDE. To reference all of them, we could call them "Fedora Immutable" or "Fedora Atomic". This could also help us reintroduce spins and editions.

## Only Ship Unfiltered Flathub by Default
[Flatpak] is pre-installed on Fedora Linux. It comes with a heavily [filtered] variant of [Flathub] and comes with [Fedora Flatpaks], Fedora's Flatpak remote.

Typically, distributions that pre-install Flatpak also pre-add an unfiltered Flathub, as it is the de-facto and most popular remote. I am personally completely against pushing Fedora Flatpaks and filtering Flathub, as it is a hostile approach in pushing Flatpak, and is unsustainable.

### The Problems With Fedora Flatpaks
Since the very beginning of Fedora Flatpaks's existence, the Fedora Project pushes it unconditionally, all without putting the effort to convince users and developers for any practical benefits. This is really harmful to Flatpak adoption and to Fedora Linux in general, as users (and even contributors) don't have useful information regarding Fedora Flatpaks and its benefits. This leaves a distasteful experience with Fedora Flatpaks to users, while believing that this is a problem with Flatpak itself (like myself when I first tried them).

Fedora Flatpaks lacks many contributors and support, as many more developers and organizations are interested in Flathub. Furthermore, there is no support room/channel related for Fedora Flatpaks. In contrast, the [Flatpak Matrix room] is a shared room for Flatpak and Flathub. So, not only is there no proper support for Fedora Flatpaks, there aren't enough contributors and developers to sustain Fedora Flatpaks.

10 months ago, I wrote an article at the Fedora Magazine to [compare and contrast Fedora Flatpaks and Flathub](https://fedoramagazine.org/comparison-of-fedora-flatpaks-and-flathub-remotes). In the "Number of applications" section, I checked the amount of applications available in Fedora Flatpaks and Flathub. At that time, Fedora Flatpaks had 86 applications available, whereas Flathub had 1518. Let's compare the numbers as of writing this article:

```terminal
$ flatpak remote-ls --app fedora | wc -l
87
$ flatpak remote-ls --app flathub | wc -l
1988
```

We notice that, 10 months later, Fedora Flatpaks only published one application, whereas the amount of applications in Flathub went from just above 1500 to almost 2000. Fedora Flatpaks was first introduced in [late 2018](https://blog.fishsoup.net/2018/12/04/flatpaks-in-fedora-now-live/). In the span of 4 years, Fedora Flatpaks still hasn't reached 100 applications.

Many developers and users do not consider Fedora Flatpaks appealing, myself included. This is dangerous for Flatpak's future, as Fedora Linux is meant to be appealing for developers. However, developers side with Flathub instead, including those who develop free and open source software. These developers will only publish their applications on Flathub thanks to Flathub's much wider coverage in support and discoverability.

Personally, I believe that Fedora Flatpaks is very anti-collaborative and hostile on the Fedora Project's end, especially when the Fedora Project has a long history of collaborating with upstream projects to advance the Linux desktop. With Flathub, we notice that GNOME, KDE, elementary, Endless Foundation, and many other free and open source organizations collaborate for Flathub development and adoption, whereas the Fedora Project is one of the exceptions.

### Why Only Flathub Should Be Pushed
We're not only noticing usability paradigm shifts, we're also noticing a community paradigm shift, where major communities with different backgrounds and goals agree, develop and adopt the same standard, instead of implementing everything internally and risking worsening fragmentation. Having the Fedora Project involved in Flathub development and adoption and dropping support for Fedora Flatpaks would seriously improve the Linux desktop (and mobile) in the long run.

We have noticed a consistent positive outcome when many communities come together and work on a single standard, notably systemd, freedesktop.org standards or even the infamous Linux kernel. When we all adopt the same standards, the improvements become exponentially larger.

From a legal perspective, Flatpak introduces a method to work around proprietary licenses and codecs, as explained in this [video](https://youtu.be/xnnJRP4t9gM?t=3221). Everything Flathub develops is entirely [free and open source](https://github.com/flathub/), so this does not go against the philosophy that the Fedora Project preaches. Furthermore, store front-ends, such as GNOME Software and KDE Discover, even have a dedicate section that explains the risks of proprietary software.

## Adwaita-Qt
When I first used Qt applications on Fedora Linux on GNOME, many of them were literally unusable, due to styling issues. This is because Fedora Linux comes with [Adwaita-Qt], "\[a\] style to bend Qt applications to look like they belong into GNOME Shell". Here are some examples of usability issues:

{% include image.html
url="/assets/Articles/where-fedora-linux-could-improve/Dolphin.webp"
caption="[Dolphin](https://apps.kde.org/dolphin) with Adwaita-Qt with Dark style. The main view displays light text on a white background; the toolbar displays dark gray icons on a dark gray background, resulting in low contrast and making the app appear broken."
%}

{% include image.html
url="/assets/Articles/where-fedora-linux-could-improve/Kate.webp"
caption="[Kate](https://apps.kde.org/kate) with Adwaita-Qt with Dark style. The majority of the app doesn't apply the dark style and sticks with light style; buttons display dark gray text on top of a dark gray background, resulting in low contrast."
%}

{% include image.html
url="/assets/Articles/where-fedora-linux-could-improve/Konsole.webp"
caption="[Konsole](https://apps.kde.org/konsole) with Adwaita-Qt with Dark style. The toolbar displays dark text on a dark background when hovering with a cursor; the toolbar and tabs don't apply the dark style and stick with light style."
%}


Initially, I had no good knowledge with Qt, so I thought these were application issues. Since the Fedora Linux community often claims that Fedora Linux provides a "vanilla" experience, the last thing I would expect is it modifying the interface and behavior of applications. Unfortunately, I was wrong -- Fedora Linux does not provide a vanilla experience, as it modifies Qt applications on GNOME.

After 30 minutes of fiddling with styling, I finally found the solution: to use Breeze, the default style in Plasma. In my case, I uninstalled Adwaita-Qt from my systems.

The main problem with styling is that it is literally impossible to be as reliable as the most common style (in this case, Breeze). Most developers test Qt applications with Breeze, without testing with other styles. Breeze itself isn't perfect, but it has the narrowest scope of breakage, but any custom style will always broaden the scope, and thus will increase the chance of breaking user experience. Shipping custom styles by default may make applications appear broken, and may even give the impression to the user that the application is at fault, which initially happened to me, as explained above.

I strongly urge the Fedora Project to stop shipping Adwaita-Qt by default. While I have no problem with custom styles existing, breaking applications *by default* is harmful to the relationship between application developers, distribution maintainers and users. The best we can do is ship however the developer tested their application, and let users break their system themselves.

## Conclusion
Fedora Linux is my favorite Linux distribution, because it does many things right, especially the amount of effort the community is putting to advocate for free and open source software and community involvement. However, it is not perfect. There are areas I certainly disagree with, like pushing Fedora Flatpaks and Adwaita-Qt.

I personally believe that we should push big organizations into collaborating with other organizations and the rest of the community, instead of potentially creating an unsustainable alternative. I also believe that we should protect the user and developer experience by not modifying the visuals of the program, to avoid conflicts and guarantee the best experience to the user.

[Fedora Workstation]: https://web.archive.org/web/20221014165625/https://getfedora.org/en/workstation
[Fedora Server]: https://web.archive.org/web/20220922010406/https://getfedora.org/en/server
[Fedora IoT]: https://web.archive.org/web/20220922055431/https://getfedora.org/en/iot
[Fedora Silverblue]: https://web.archive.org/web/20221013234747/https://silverblue.fedoraproject.org
[Fedora Kinoite]: https://web.archive.org/web/20221021181506/https://kinoite.fedoraproject.org
[Fedora CoreOS]: https://web.archive.org/web/20221001090513/https://getfedora.org/coreos
[Fedora KDE Plasma Desktop Edition]: https://spins.fedoraproject.org/kde
[Flatpak]: https://flatpak.org
[Flathub]: https://flathub.org
[filtered]: https://pagure.io/fedora-flathub-filter/blob/main/f/fedora-flathub.filter
[Fedora Flatpaks]: https://fedoramagazine.org/an-introduction-to-fedora-flatpaks
[Flatpak Matrix room]: https://matrix.to/%23/%23flatpak%3Amatrix.org
[Adwaita-Qt]: https://github.com/FedoraQt/adwaita-qt

---

- Edit 1: Update Flathub information (Credit to [Cassidy James](https://cassidyjames.com))
- Edit 2: Correct typos and improve sentences
