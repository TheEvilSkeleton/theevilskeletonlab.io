---
layout: post
title: "Why You Shouldn’t Recommend Manjaro to Newcomers"
description: "Manjaro is often considered as a good entry-level distribution for those switching from Windows. However, there are many issues regarding this recommendation — most of which lie in the areas of ethicality, technicality, and general bad practices. This post tackles the misinformation that the Manjaro team and/or the individuals within the Manjaro community often spread to attract users to use Manjaro. Many of these issues impact several aspects of Manjaro."
toc: true
tags: ["Linux"]
disclaimer: "This post is opinionated. However, I try my best to provide evidence and objective reasoning. I haven't consulted any Manjaro developer/contributor either. And lastly, I am not a security researcher."
redirect_from: /2021/12/05/why-you-shouldnt-recommend-manjaro-to-newcomers.html
---

## Introduction

{{ page.description }}

## Note

Just to be clear, "Manjaro" will be used to refer to the distribution, "Manjaro team" will be used to refer to the [Manjaro team](https://manjaro.org/team/), and "Manjaro community" will be used to refer to the distribution's userbase.

## Security and Stability
Security is a huge issue on Manjaro. Giving a large benefit of doubt, it is extremely difficult to balance security with convenience. What is seemingly harder is to balance them with stability and performance, especially when there are many factors when it comes to what a user prioritizes. However, the average user is unlikely to have knowledge on threat modeling or even the fundamental basics of security, meaning that something robust and meaningful to the user should be set up without user interaction.

Fedora, Ubuntu, Pop!\_OS and some other distributions take the precautionary extra steps to improving the security by default: Fedora heavily promotes Flatpak and enforces SELinux policies, and Ubuntu and Pop!\_OS enforce AppArmor profiles and promote Snap and Flatpak respectively. While it seems that Manjaro, on the other hand, is very permissive when it comes to security and has many issues with delivering packages.

### Permissive Policies
Let's start with policies. Manjaro uses AppArmor as the mandatory access control (MAC): a security feature that gives users control over what data a program has access to, for example a directory, external devices and more. Debian, Ubuntu, Pop!_OS and openSUSE use AppArmor due to its user-friendly and powerful nature when used properly.

On a default, fully updated Manjaro GNOME install, running `aa-status` outputs:
```terminal
$ sudo aa-status
apparmor module is loaded.
7 profiles are loaded.
7 profiles are in enforce mode.
   /usr/lib/snapd/snap-confine
   /usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   dovecot-script-login
   lsb_release
   nvidia_modprobe
   nvidia_modprobe//kmod
   php-fpm
0 profiles are in complain mode.
0 profiles are in kill mode.
0 profiles are in unconfined mode.
0 processes have profiles defined.
0 processes are in enforce mode.
0 processes are in complain mode.
0 processes are unconfined but have a profile defined.
0 processes are in mixed mode.
0 processes are in kill mode.
```

On Ubuntu and Pop!_OS:
```terminal
$ sudo aa-status 
apparmor module is loaded.
25 profiles are loaded.
23 profiles are in enforce mode.
   /usr/bin/evince
   /usr/bin/evince-previewer
   /usr/bin/evince-previewer//sanitized_helper
   /usr/bin/evince-thumbnailer
   /usr/bin/evince//sanitized_helper
   /usr/bin/man
   /usr/lib/NetworkManager/nm-dhcp-client.action
   /usr/lib/NetworkManager/nm-dhcp-helper
   /usr/lib/connman/scripts/dhclient-script
   /usr/lib/cups/backend/cups-pdf
   /usr/sbin/cups-browsed
   /usr/sbin/cupsd
   /usr/sbin/cupsd//third_party
   /{,usr/}sbin/dhclient
   libreoffice-senddoc
   libreoffice-soffice//gpg
   libreoffice-xpdfimport
   lsb_release
   man_filter
   man_groff
   nvidia_modprobe
   nvidia_modprobe//kmod
   tcpdump
2 profiles are in complain mode.
   libreoffice-oosplash
   libreoffice-soffice
0 profiles are in kill mode.
0 profiles are in unconfined mode.
2 processes have profiles defined.
2 processes are in enforce mode.
   /usr/sbin/cups-browsed (727) 
   /usr/sbin/cupsd (701) 
0 processes are in complain mode.
0 processes are unconfined but have a profile defined.
0 processes are in mixed mode.
0 processes are in kill mode.
```

On openSUSE:
```terminal
~> sudo aa-status 
apparmor module is loaded.
50 profiles are loaded.
49 profiles are in enforce mode.
   /usr/bin/lessopen.sh
   /usr/lib/colord
   apache2
   apache2//DEFAULT_URI
   apache2//HANDLING_UNTRUSTED_INPUT
   apache2//phpsysinfo
   avahi-daemon
   dnsmasq
   dnsmasq//libvirt_leaseshelper
   dovecot
   dovecot-anvil
   dovecot-auth
   dovecot-config
   dovecot-deliver
   dovecot-dict
   dovecot-dovecot-auth
   dovecot-dovecot-lda
   dovecot-dovecot-lda//sendmail
   dovecot-imap
   dovecot-imap-login
   dovecot-lmtp
   dovecot-log
   dovecot-managesieve
   dovecot-managesieve-login
   dovecot-pop3
   dovecot-pop3-login
   dovecot-script-login
   dovecot-ssl-params
   dovecot-stats
   ghostscript
   identd
   klogd
   lsb_release
   mdnsd
   nmbd
   nscd
   ntpd
   nvidia_modprobe
   nvidia_modprobe//kmod
   php-fpm
   ping
   samba-bgqd
   smbd
   smbldap-useradd
   smbldap-useradd///etc/init.d/nscd
   syslog-ng
   syslogd
   traceroute
   winbindd
1 profiles are in complain mode.
   ghostscript///usr/bin/hpijs
0 profiles are in kill mode.
0 profiles are in unconfined mode.
2 processes have profiles defined.
2 processes are in enforce mode.
   /usr/sbin/avahi-daemon (636) avahi-daemon
   /usr/sbin/nscd (674) nscd
0 processes are in complain mode.
0 processes are unconfined but have a profile defined.
0 processes are in mixed mode.
0 processes are in kill mode.
```

Here, Manjaro only enforces 7 profiles. In contrast Ubuntu and Pop!\_OS enforce about 23 profiles, and openSUSE enforces 49 profiles. With openSUSE and Ubuntu/Pop!\_OS, many network related profiles are enforced, like NetworkManager and dhclient. Furthermore, Debian packages include AppArmor profiles and get loaded automatically, which Ubuntu and Pop!\_OS take advantage of that. This is ultimately essential because the network, e.g. web, other systems in the network, etc. are an easy target for bad actors, and not having a decently hardened system can be easily compromised.

Manjaro only enforces few policies, which makes it extremely insecure. Manjaro enforces some AppArmor profiles because Snap requires AppArmor to properly isolate applications, which is completely understandable, but Snap is not the main package manager to begin with, i.e. it is not used by default. Manjaro also comes with Flatpak, but just like Snap, it's unused by default.

To sum it up, Manjaro does not take the extra steps to enforce policies. Ubuntu and Pop!\_OS, two other user-friendly distributions that use AppArmor, enforce a lot more than Manjaro. Further, Ubuntu and Pop!_OS heavily promote Snap and Flatpak respectively. While Snap has many issues, it does use AppArmor to sandbox applications. It is really important that a distribution is well hardened when it is targeted to inexperienced users because these users often lack knowledge in security.

### Delayed Updates
Manjaro often delays packages for a couple weeks, but the delays cause security issues and are unlikely to actually benefit stability for a couple of reasons.

#### Security regression
Manjaro states that they delay packages for "roughly a couple of weeks".<sup>[(Source)](https://web.archive.org/web/20211105141705/https://wiki.manjaro.org/index.php/Switching_Branches#:~:text=stable%20branch%3A%20the%20packages%20that%20come%20to%20stable%20have%20gone%20through%20roughly%20a%20couple%20of%20weeks%20testing%20by%20the%20users%20of%20the%20unstable%2Ftesting%20repos%2C%20before%20they%20get%20the%20packages.%20these%20packages%20are%20usually%20free%20of%20any%20problems.)</sup> Those weeks of delays are used mostly for testing purposes, which is completely reasonable. However, Manjaro is based on Arch Linux, a bleeding edge distribution. Bleeding edge distributions update packages more often than stable distributions. These updates may include security updates along the way. With the weeks of "testing," Manjaro may deliver security updates a couple of weeks later than Arch Linux and many other bleeding edge distributions.

Debian for example is notorious for delaying packages as well. However, Debian is an LTS distribution, meaning major updates are supported for a long time with minor updates, and generally get less updates. Despite the delays, Debian is regarded as one of the most secure distributions today. That is because Debian backports security patches and then releases those packages to the public, meaning only security elements are included with the packages, and then pushed to the public so the vulnerabilities are mitigated as much as possible. But, Manjaro does not backport security patches, so what we're getting in general is packages that are likely to contain many more security vulnerabilities for a couple more weeks.

#### Unproven Claims about Stability
The Manjaro community often claims to be a more stable version of Arch Linux because packages are delayed and "tested".<sup>([1](https://www.fosslinux.com/44287/manjaro-vs-arch-linux.htm#:~:text=One%20big%20feature,the%20long%20run.), [2](https://itsfoss.com/manjaro-vs-arch-linux/#:~:text=a%20consequence%20of%20accommodating%20this%20testing%20process%20is%20that%20manjaro%20will%20never%20be%20quite%20as%20bleeding-edge%20as%20arch.%20but%20then%2C%20it%20makes%20manjaro%20slightly%20more%20stable%20than%20arch%20and%20less%20susceptible%20to%20breaking%20your%20system.), [3](https://linuxiac.com/manjaro/#:~:text=To%20ensure%20continued,general%20user%20base.))</sup> But, how can the Manjaro team test all their packages when the team is relatively small? Since Manjaro targets inexperienced users, how big is the community involvement in quality assurance (QA)? And lastly, where are the evidence that what Manjaro is providing is actually more stable in the real world than Arch Linux?

The Manjaro team has 13 thousand packages to test with the default repositories,<sup>([1](#1))</sup> many of which are core packages and essential packages. Not only that, if they ever test all those 13 thousand packages, then this would only be tested on one system. Already that testing software on LTS distributions is really difficult and takes a considerably long time, Manjaro has to deal with 13 thousand bleeding-edge packages that are regularily updated, which makes it literally impossible to efficiently test, especially when it's managed by a small community.

Debian for example has a much larger community that actively tests packages<sup>([2](#2))</sup>, which Ubuntu, Pop!\_OS and other Debian based distributions base the effort from. Fedora as well has a large community<sup>([2](#2))</sup>, and introduces Test Days for contributors to test upgrades, latest kernels and changes by people that uses a large scope of hardware.<sup>[(Source)](https://fedoraproject.org/wiki/QA/Test_Days)</sup> Many users that use community-driven distributions also contribute back to the projects. And those that do contribute may contribute more than once. Those distributions are relatively stable because a large part of the community actively tests with different hardware to make distributions as stable as stable as possible. Even if it's impossible to test every single combination in the world, the testing done by large communities influences the stability of distributions in many ways.

Briefly put, the Manjaro team is likely too little to properly test. The stability improvements the Manjaro community claims is hardly a benefit in practice because they try to do too much when they are too small. The amount of time and effort contributors ideally have to put in to properly test packages will need several weeks more of testing on top of the weeks of delay they are currently doing. And even then, delaying packages any further may expose to security risks.

### Security Overlay
The [Security Overlay](https://gitlab.manjaro.org/security-overlay) is an effort to ensure important packages are kept up-to-date with upstream to mitigate security issues on popular packages like Firefox.<sup>[(Source)](https://gitlab.manjaro.org/security-overlay#:~:text=group%20for%20short-term%20packaging%20changes%20for%20security-related%20updates%20or%20fast-tracked%20overlay%20packages.)</sup> It is a group where all packages that are tampered for security purposes temporarily stay in that group. After the work is done, it is archived temporarily and then removed from that group. (From what I understood.)

Regardless, because of the reasons I explained in the previous paragraphs, the Security Overlay has its own limitations. The effectiveness depends a lot on the Manjaro contributors and number of packages available. Given that Manjaro doesn't have many contributors and has to maintain 13 thousand packages that are actively changing, it is far from mitigating that issue. Albeit it's limited, it's a step in the right direction.

### AUR
The Arch User Repository (AUR) is an unofficial repository maintained by the Arch community. It was built specifically with Arch Linux in mind, but it works on most Arch based distributions because most of these distributions use the official Arch repositories. Manjaro, on the other hand, may have a lot of issues with the AUR since it delays packages for several weeks and because they are using a custom repository with patches packages here and there. The AUR assumes that the user is using the latest versions of packages, which Manjaro often doesn't, so it has a bigger chance of having negative side effects or even breaking existing systems later down the road.

## Promoting the AUR
The Manjaro community often promotes the use of the AUR. As mentioned [previously](#aur), there may be negative side effects when using the AUR on Manjaro because the AUR assumes the system is running up-to-date software from the Arch Linux repositories which Manjaro systems often don't. Even with this problematic issue, the Manjaro community heavily promotes the AUR regardless of its issues, and some don't even warn about the risks.

Furthermore, Manjaro does not provide support for the AUR.<sup>[(Source)](https://wiki.manjaro.org/index.php/Arch_User_Repository#:~:text=no%20support%20will%20be%20provided%20by%20the%20manjaro%20team%20for%20any%20issues%20that%20may%20arise%20relating%20to%20software%20installations%20from%20the%20aur.%20when%20manjaro%20is%20updated%2C%20aur%20packages%20might%20stop%20working.%20this%20is%20not%20a%20manjaro%20issue)</sup> While this is fine, the AUR is easily accessible in Pamac, even more so than Arch Linux itself because all the user needs to do is to enable AUR support in Pamac.

## Treasurer Incident
In 2019, Philip Müller, the lead developer of Manjaro, wanted to use the Manjaro funds that were donated by the community to buy someone a laptop for €2,000.<sup>[(Source)](https://opencollective.com/manjaro/expenses/22477)</sup> Jonathon, the former treasurer of Manjaro, rejected the investment because it violated the Manjaro policies — without prior consultations with other members. This led to a debate between Philip and Jonathon, which ended with Jonathon resigning from treasurer position.<sup>([1](https://archived.forum.manjaro.org/t/change-of-treasurer-for-manjaro-community-funds/154888) [(Archived)](https://web.archive.org/web/20200807042341/https://forum.manjaro.org/t/change-of-treasurer-for-manjaro-community-funds/154888), [2](https://archived.forum.manjaro.org/t/change-in-manjaro-team-composition/155231)) (Kudos to Manjaro for not deleting sources)</sup>

This news was widespread in the Linux community. Many people, including myself, have questioned Manjaro's future and are still wondering how Manjaro will be managing the funds and what they will be spending from here on out. Manjaro already doesn't financially contribute to Arch Linux but they instead decided to spend on a gaming laptop. The fact that the lead developer violated Manjaro's own policies and even decided to debate with the trasurer is extremely shady, in my opinion. What's the point of having policies if they are violated by those that sincerely agreed on?

## Conclusion
**I urge publications and the Linux community to stop recommending Manjaro.** Claiming that something is more stable only because it delays packages for testing purposes is only a baseless claim without any evidence of it actually being true. There are many things to look out for when testing packages, e.g. what is tested, how many packages are tested, how long they are tested, etc. Manjaro is a "too good to be true" distribution; there are many flaws revolving the Manjaro team and the distribution itself that is often not being discussed.

Having to properly test 13 thousand packages in an environment where packages frequently change on a daily basis when there is a lack of community involvement in quality assurance and when the team is relatively small is not only impossible, but is unlikely to show a difference in comparison to Arch Linux in terms of stability, and likely be even worse. The AUR is proactively promoted in the community but when an issue occurs that only happens on Manjaro because, e.g. a package in the AUR is dependent to a specific package to be up-to-date that Manjaro does not ship it yet, there is very little to no support. Also, deliberately violating the internal policies and then arguing about it without actually acknowledging the poor behavior not only makes the policies redundant to some extent, it also shows their true colors.

Telling an average user to install a fully fledged operating system that is prone to breakage, doesn't back their claims, pushes bad practices, and doesn't take security seriously is questionable and should be immediately disqualified as a good distribution for the average user.

### Alternatives

There are some distributions targeted for the average user. Here are the two distributions I recommend:
- [Pop!\_OS](https://pop.system76.com/) (for NVIDIA GPUs): Based on Ubuntu, but attempts to promote open standards like Flatpak. There are also ISOs that come with NVIDIA drivers pre-installed;
- [Fedora Workstation](https://getfedora.org/) (for AMD/Intel GPUs): Fedora is very similar in regards to what is being promoted. However, Fedora takes a step further by using cutting edge technologies while staying stable, and these technologies are generally friendlier on AMD/Intel GPUs because of their open source nature.

Both distributions are backed by large communities and are overall stable.

If you are interested in an Arch based distribution, then I suggest using [ArcoLinuxB](https://www.arcolinux.info/downloads/) since it lets you choose the desired desktop, or simply [Arch Linux](https://archlinux.org/download/). I also suggest using and learning more about [AppArmor](https://wiki.archlinux.org/title/AppArmor) since it's unused by default and fairly easy to get started.

---

<p id="1">1: On a Manjaro install, run <code class="language-plaintext highlighter-rouge">pacman --sync --search --quiet | wc --lines</code> in the terminal</p>

<p id="2">2: Debian and Fedora have had more than 1 thousand contributors in 2021 and 2 thousand contributors in 2016 respectfully<sup>(unable to find recent data)</sup>: <a href="https://web.archive.org/web/20211026011539/https://contributors.debian.org">https://web.archive.org/web/20211026011539/https://contributors.debian.org</a>; <a href="https://mattdm.org/fedora/2016flock/StateofFedoraAugust2016-v160731a.pdf">https://mattdm.org/fedora/2016flock/StateofFedoraAugust2016-v160731a.pdf</a></p>

Edit 1: Mention that Debian packages include AppArmor profiles
