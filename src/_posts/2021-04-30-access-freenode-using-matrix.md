---
title: "Access freenode Using Matrix Clients"
layout: post
toc: true
tags: ["IRC", "Matrix"]
description: "
*Article originally posted on [Fedora Magazine](https://fedoramagazine.org/access-freenode-using-matrix-clients/).*


Matrix (also written [matrix]) is an [open source project](https://matrix.org/) and a [communication protocol](https://matrix.org/docs/spec/). The protocol standard is open and it is free to use or implement. Matrix is being recognized as a modern successor to the older [Internet Relay Chat (IRC)](https://en.wikipedia.org/wiki/Internet_Relay_Chat) protocol. [Mozilla](https://matrix.org/blog/2019/12/19/welcoming-mozilla-to-matrix/), [KDE](https://matrix.org/blog/2019/02/20/welcome-to-matrix-kde/), [FOSDEM](https://matrix.org/blog/2021/01/04/taking-fosdem-online-via-matrix) and [GNOME](https://wiki.gnome.org/Initiatives/Matrix) are among several large projects that have started using chat clients and servers that operate over the Matrix protocol. Members of the Fedora project have [discussed](https://discussion.fedoraproject.org/t/the-future-of-real-time-chat-discussion-for-the-fedora-council/24628) whether or not the community should switch to using the Matrix protocol.


The Matrix project has implemented an IRC bridge to enable communication between IRC networks (for example, [freenode](https://en.wikipedia.org/wiki/Freenode)) and [Matrix homeservers](https://en.wikipedia.org/wiki/Matrix_(protocol)#Servers). This article is a guide on how to register, identify and join freenode channels from a Matrix client via the [Matrix IRC bridge](https://github.com/matrix-org/matrix-appservice-irc).


Check out *[Beginner’s guide to IRC](https://fedoramagazine.org/beginners-guide-irc/)* for more information about IRC.
"
redirect_from: /2021/04/30/access-freenode-using-matrix.html
---

## Introduction

{{ page.description }}

## Preparation

You need to set everything up before you register a nick. A nick is a username.

## Install a Client

Before you use the IRC bridge, you need to install a Matrix client. This guide will use Element. Other [Matrix clients](https://matrix.org/clients/) are available.

First, install the Matrix client *Element* from [Flathub](https://flathub.org/apps/details/im.riot.Riot) on your PC. Alternatively, browse to [element.io](https://app.element.io/) to run the Element client directly in your browser.

Next, click *Create Account* to register a new account on matrix.org (a homeserver hosted by the Matrix project).

## Create Rooms

For the IRC bridge, you need to create rooms with the required users.

First, click the **+** (plus) button next to *People* on the left side in Element and type `@appservice-irc:matrix.org` in the field to create a new room with the user.

Second, create another new room with *@freenode_NickServ:matrix.org*.

## Register a Nick at freenode

If you have already registered a nick at freenode, skip the remainder of this section.

Registering a nickname is optional, but strongly recommended. Many freenode channels require a registered nickname to join.

First, open the room with *appservice-irc* and enter the following:

```
!nick <your_nick>
```

Substitute `<your_nick>` with the username you want to use. If the nick is already taken, *NickServ* will send you the following message:

```
This nickname is registered. Please choose a different nickname, or identify via /msg NickServ identify <password>.
```

If you receive the above message, use another nick.

Second, open the room with *NickServ* and enter the following:

```
REGISTER <your_password> <your_email@example.com>
```

You will receive a verification email from freenode. The email will contain a verification command similar to the following:

```
/msg NickServ VERIFY REGISTER <your_nick> <verification_code>
```

Ignore `/msg NickServ` at the start of the command. Enter the remainder of the command in the room with `NickServ`. Be quick! You will have 24 hours to verify before the code expires.

## Identify Your Nick at freenode

If you just registered a new nick using the procedure in the previous section, then you should already be identified. If you are already identified, skip the remainder of this section.

First, open the room with `@appservice-irc:matrix.org` and enter the following:

```
!nick <your_nick>
```

Next, open the room with `@freenode_NickServ:matrix.org` and enter the following:

```
IDENTIFY <your_nick> <your_password>
```

To join a freenode channel, press the **+** (plus) button next to *Rooms* on the left side in Element and type `#freenode_#<your_channel>:matrix.org`. Substitute `<your_channel>` with the freenode channel you want to join. For example, to join the `#fedora` channel, use `#freenode_#fedora:matrix.org`. For a list of Fedora Project IRC channels, see *[Communicating\_and\_getting\_help — IRC\_for\_interactive\_community\_support](https://fedoraproject.org/wiki/Communicating_and_getting_help#IRC_for_interactive_community_support)*.

## Addendum – June 1st, 2021

Due to the [recent announcement](https://fedoramagazine.org/irc-announcement-fedora-moving-to-libera-chat/), many projects have switched to Libera Chat, including Fedora.

To join Libera Chat channels, make the following substitutions when using this guide:

- @appservice-irc:matrix.org → @appservice:libera.chat
- @freenode\_NickServ:matrix.org → @NickServ:libera.chat
- appservice-irc → appservice
- freenode\_#<your\_channel>:matrix.org → #<your\_channel>:libera.chat
- freenode\_#fedora:matrix.org → #fedora:libera.chat
