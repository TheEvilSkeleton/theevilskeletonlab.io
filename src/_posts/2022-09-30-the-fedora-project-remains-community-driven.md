---
layout: post
title: "The Fedora Project Remains Community Driven"
description: "Recently, the Fedora Project removed all patented codecs from their Mesa builds, without the rest of the community's input. This decision was heavily criticized from the community. For that decision, some even [asked](https://www.reddit.com/r/Fedora/comments/xqqp7c/please_remove_the_community_driven_part_from/) the Fedora Project to remove \"community driven\" from its official description. I'd like to spend some time to explain why, in my opinion, this decision was completely justified, and how the Fedora Project remains community driven."
tags: ["Linux", "Fedora"]
toc: true
redirect_from: /2022/09/30/the-fedora-project-remains-community-driven.html
---

## Introduction

{{ page.description }}

## Law Compliance Cannot Be Voted
Massive organizations, like the Fedora Project, must comply with laws to avoid lawsuits as much as possible. [Patent trolls] are really common and will target big organizations.
Let's not forget that, in 2019, GNOME was sued by a [patent troll](https://en.wikipedia.org/wiki/GNOME_Foundation#GNOME_Patent_Troll_Defense_Fund). Unfortunately, patent trolling is quite common. And even worse, patent trolling against open source projects has considerably [increased] since early this year. So, this decision had to be acted quickly, to avoid potential lawsuits as soon as possible.

Complying with laws is not up to the community to decide. For example, Arch Linux, another community driven distribution, cannot and will not redistribute proprietary software, unless they have the permission to do so from the authors of the software. And this is not something that can be voted on, but must be complied with. This doesn't mean that Arch Linux is not community driven whatsoever; it only means that it's legally bound, just like how the Fedora Project cannot ship these patented codecs.

Even if the Fedora Project wasn't sued in the past years, it doesn't mean that they will continue to be free of lawsuits in the future. The increase in patent trolling is a good reason for the Fedora Project to quickly react on this. If they ever get sued, is the community going to pay for lawyers?

## Community Driven
As a volunteer of the Fedora Project who is unaffiliated with Red Hat, I believe that the Fedora Project remains community driven. I am currently a part of [Fedora Websites & Apps Team] with the ["developer" role] of the upcoming [website revamp repository]. This is mainly a volunteer effort, as the majority of us contributing to it are unaffiliated with Red Hat and unpaid developers.

Since we (volunteers) are the ones in control with the decision, we could intentionally make the website look displeasing and appalling. Of course, we care about the Fedora Project, so we want it to look appealing for potential users, contributors and even enterprises that are willing to switch to open source.

Recently, I [proposed] to unify [Silverblue], [Kinoite], [CoreOS], and other pages' layouts into one that looks uniform and consistent when navigating, e.g. same navigation bar, footer, color palette, etc. Some developers are considering joining the effort, but some disagree. Of course, this is merely a proposal, but if everyone is on board, then we volunteers will be the ones leading this initiative.

This is one example from personal experience, but many initiatives were (and will be) proposed by independent contributors, and can also lead the effort. Nonlegal proposals are still democratically voted and surveys are still taken seriously. Currently, the Fedora Project is in the process of migrating from Pagure to GitLab, and from IRC to Matrix. That is because the community voted on it. I voiced my opinion and was one of the people who proposed both of those changes in the surveys.

## Conclusion
I completely agree with the Fedora Project's decision on disabling patented codecs from Mesa. These changes cannot and should not be asked by the community, as this is a legal discussion about potential lawsuits. Anything that is nonlegal remains democratically voted by the community, as long as you comply with US laws (unfortunately) and the Fedora Code of Conduct.

---
Edit 1: Use Arch Linux as an example instead of Gentoo Linux, as it is a binary based distribution

Edit 2: Mention exception for Arch Linux (Credit to [array](https://fosstodon.org/web/@array/109097358815637311) and [u/Ursa_Solaris](https://www.reddit.com/r/Fedora/comments/xvj847/comment/ir2j8dh/?context=3))

[Fedora Websites & Apps Team]: https://docs.fedoraproject.org/en-US/websites/
[Fedora Magazine]: https://docs.fedoraproject.org/en-US/fedora-magazine/editorial-meetings/#:~:text=chair%20glb%20rlengland-,theevilskeleton,-cverna%20asamalik
[Patent trolls]: https://en.wikipedia.org/wiki/Patent_troll
[increased]: https://www.zdnet.com/article/patent-troll-attacks-against-open-source-projects-are-up-100-since-last-year-heres-why/
[website revamp repository]: https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0
["developer" role]: https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0/-/project_members?search=TheEvilSkeleton
[Silverblue]: https://silverblue.fedoraproject.org/
[Kinoite]: https://kinoite.fedoraproject.org/
[CoreOS]: https://getfedora.org/en/coreos
[proposed]: https://github.com/fedora-silverblue/silverblue-site/issues/114#issuecomment-1246879559
