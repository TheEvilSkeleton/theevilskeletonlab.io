---
layout: post
title: "Problems With Not Raising Awareness and Protecting Marginalized Groups in FOSS"
description: "A common problem I notice on the internet is the lack of awareness of many problems in the world. Of which, there are times when someone may be aware of the problem, but not to what extent. One of the common problems is a lack of effort to raise awareness and protect marginalized groups, particularly in the free and open-source software (FOSS) community.\n\nIn this article, we're going to go over the problems associated with not raising awareness and protecting marginalized groups. I'll also go over the \"keep politics out of FOSS\" demand from many free software activists who oppose raising awareness."
tags: ["Politics"]
toc: true
trigger: "This post contains mentions of discrimination and harassment."
disclaimer: "
1. This article is not a discussion about whether the entities in examples have made the right decisions, but rather their intentions.

1. Please note that this article is not about offering solutions, to keep it specific to the title. It is only about pointing out the problems. Solutions could be written by someone else or in a future article.

1. While this article is meant to highlight the issues with marginalized groups, I completely acknowledge that there are public \"inclusive\" spaces that discriminate against groups typically seen as non-marginalized, such as white, heterosexual and cisgender people. We should put in the same effort to ensure they feel included and protected from hatred."
redirect_from: /2023/07/02/problems-with-not-raising-awareness-and-protecting-marginalized-groups-in-foss.html
---

## Introduction

{{ page.description }}

## What Is a "Marginalized Group"?

Before I go over the importance of protecting marginalized groups, allow me to explain the meaning of a marginalized group. "Marginalized" means being excluded in bad faith[^1] or being viewed as an insignificant. A marginalized group is a group of people that is excluded in bad faith and/or treated as a less important group. Some examples of marginalized groups include but are not limited to:
- sexuality and gender
- disability
- elderly or young (age)
- immigrant status
- religion
- socioeconomic status

For a list of types of discrimination, see the [Discrimination sidebar](https://en.m.wikipedia.org/wiki/Template:Discrimination_sidebar) on Wikipedia.

## Lack of Awareness May Result in Not Providing Necessary Accommodations

There are many underrepresented groups that are unfortunately easily forgotten, not taken into consideration, never consulted, and/or not prioritized. This may lead to not providing any accommodations until developers are made aware. One group I can think of is people with disabilities, such as blind people.

We all know that people with disabilities exist, but when we have many things to worry about, it may become easy to forget about them. For example, my website's [accessibility] (usability for people with disabilities) used to be far from ideal: 
- Media (images/videos) lacked descriptions for screen readers or unstable internet connection.
- Many [decorative images] and elements weren't ignored by screen readers. This means that the screen reader user would have their time wasted on the screen reader explaining unnecessary information.
- Some areas were low on contrast, like code blocks. This would make it difficult for vision-impaired readers to read the content.
- A "[Skip to main content]" button was missing. This button is useful for keyboard users that don't want to go through the navigation bar.
- There weren't any accommodations for people who rely on [reduced motion]. Buttons were animating no matter the preference.

All these accommodations happened later, because I didn't take them into consideration when I made the website initially.

When I gained more experience with web development and chatted with people with disabilities, that's when I was made *aware* of it. I researched a lot about accessibility and slowly started accommodating more people. I trained myself to write better descriptions for media and faster.

This isn't only from a maintainer/developer perspective, but from a user perspective as well. Have you ever wondered why buttons on GNOME are large? Not only is it practical for touchscreen users, but it's also useful for people whose hands constantly fidget, like mine. Many people struggle with clicking accurately, so having large(r) buttons helps us click the right buttons without having to put excessive effort into moving our mice. That's also one reason why GNOME removed [status icons](https://blogs.gnome.org/aday/2017/08/31/status-icons-and-gnome) a while back (under "*4. Accessible click targets*").

Many users believe that some visual designs exist just to copy from others, look unique just for the sake of uniqueness, etc. However, many design elements are strictly focused on people who struggle with certain cues, so it's important that users are also made aware of these accommodations, and hopefully sympathize with these decisions.

## Contributors Are Driven Away by Harassment

There are many efforts put into protecting and raising awareness of marginalized groups in FOSS, but, in my opinion, it's still not enough.

A good example of an organization that protects marginalized groups is GNOME. GNOME even goes as much as making a statement about putting marginalized people's safety in priority on its [code of conduct](https://wiki.gnome.org/Foundation/CodeOfConduct):

> The GNOME community prioritizes marginalized people's safety over privileged people's comfort
> 
> [...]
> 
> Outreach and diversity efforts directed at underrepresented groups are permitted under the code of conduct. For example, a social event for women would not be classified as being outside the Code of Conduct under this provision.

...but would GNOME be a good example? Since GNOME is a huge player in the FOSS space and while it protects marginalized groups, it is also one of the largest targets for malicious actors. As such, these actors may harass marginalized people outside of GNOME spaces.

During my course of interactions, I've chatted with a few victims who stopped involving with GNOME because of the constant harassment (outside of GNOME). One of them shared the following to me:

{% include image.html
url="/assets/Articles/problems-with-not-raising-awareness-and-protecting-marginalized-groups-in-foss/harassment.webp"
caption='Message from former GNOME Foundation member: "*I had to de-list from planet GNOME because of the threats, harassment, and utter garbage I was getting both in my blog comments and sent to me via other channels and I just let my foundation membership lapse and stopped being an active GNOME contributor.* ☹️ *I get that occasionally in other open source projects like even in Fedora, but it was really next level in terms of nastiness and volume... GNOME is just super high-visible and/or somehow is a magnet for these people*".'
%}

Obviously, the GNOME Foundation does everything it can within its resources to protect marginalized groups, but even then, the weight of attacks may unfortunately outweigh GNOME's efforts. It really shows that, despite putting a lot of effort to increase awareness and protect marginalized groups, it may still not be enough. It doesn't help that the FOSS ecosystem lacks contributors (code, documentation, moderation, etc.). Pushing a person away one at a time only worsens the situation as a whole.

## Emotionally Draining

With the lack of awareness among tons of people, it can be very difficult to defend or explain themselves in environments that cannot relate with or understand them.

Furthermore, some even go as much as understating. Understating means to make a problem look like it's little of importance. For example, a disabled person might say that they can't use Linux, as it lacks tons of accessibility accommodations. Someone else could reply with "Oh come on, just take these 15 \[complicated\] steps and everything should work fine. You're just being oversensitive." Since the non-disabled person either refuses or is unable to sympathize, they might resort to understating. Even worse, many people with disabilities/disorders may pretend to be "normal", [[1]](https://en.wikipedia.org/wiki/Pretending_to_be_Normal) [[2]](https://d393uh8gb46l22.cloudfront.net/wp-content/uploads/2018/06/ATTN_10_15_ExhaustingPretending.pdf) when they're in an environment that they wouldn't fit if they expressed themselves, or they're in an environment that doesn't actively showcase that they're protected.

If you've done customer service or technical support, you probably have gotten really annoyed to continuously repeat yourself to each person on a day-to-day basis. You've also probably stumbled upon the article "[Don't ask to ask, just ask]", as the article explains a really common behavior when requesting for support. Or, if you haven't done customer service or technical support, then there's probably going to be a few things you continuously repeat to people, perhaps even to your parents when they don't know how to use their phone.

I'm already exhausted with explaining over 100 times to people that Flatpak isn't some corporate overlord that wants to take over the Linux desktop. I can't imagine how heartbreaking it may be for a marginalized person to continuously repeat themselves about their identities, disabilities, struggles or being a target for bad actors.

## "Keep Politics Out of FOSS"

The most common demand we hear from opponents of these initiatives are "keep politics out of FOSS". By "politics", they mean "any kind of politics that is out of scope of the [free software movement]", which means that FOSS should only be about promoting open and redistributable code, not alongside other activism.

The main problem with solely focusing on the "open and redistributable code" element is that we're making the entirety of FOSS inaccessible for everyone else, including potential developers. FOSS is not only about open and redistributable code; it's also about software development, because without software development, there isn't software; without software, there isn't FOSS.

Remember: software development is much more than just about maintaining code; it's about designing, marketing, documenting, and testing as well. Then, these processes consist of designers, marketers, documenters, and quality assurance testers, respectively.

If we keep politics out of FOSS, then we'd literally be excluding all the aforementioned groups, excluding developers and maintainers; we'd be excluding tons of potential code contributors as well.

The only reason you and I are using Linux in the first place is because there were initiatives that prioritized approachability, which are politically outside of the scope of the "open and redistributable code" philosophy. If it weren't for these initiatives and internal activism, then we wouldn't be using FOSS at all, and FOSS would've been long gone. Either that, or we wouldn't be on Linux because it would've been too difficult or inconvenient.

Each project has goals they want to reach, with one or multiple target audiences. So now, not only do we have the group of people within the software development space, we have our target audience(s) to take into consideration as well; we need to make it approachable for them. For example, one of GNOME's goals is to be as inclusive as possible for contributors, especially for marginalized people and underrepresented groups. As such, GNOME does whatever it can (within reason) to make it appealing for that target audience; as a result, it protects marginalized people.

"Keep\[ing\] politics out of FOSS" is something that is always bound to fail, as FOSS is by nature political beyond open and redistributable code. We need groups from different backgrounds and skills to make FOSS as appealing, useful, and work as best as it can. We need marketers to make the software deliver value to their target audience(s). We need designers to make it look well for their target audience(s). We need documenters to provide self-service and make the development experience as easy as possible. We need testers to eliminate as many bugs as possible. **Without politics, there is no FOSS.**

One particular person I admire is a 16-year-old trans girl who is a member of the GNOME Foundation. She has made significant contributions to GNOME and implemented a feature that I have wanted for a long time. Unfortunately, I can't go into detail because she's not comfortable revealing her age and name together publicly, as she doesn't want to be discriminated against and harassed outside of GNOME spaces --- rightfully so. She probably wouldn't be here if she didn't feel protected in her environment.

I suggest reading "*[Politics in your FOSS project? It's more likely than you think!]*" by Samsai. Their article goes into detail on this topic.

## Conclusion

Unfortunately, discrimination and harassment are still common in the FOSS community. Even when large communities make a real effort to protect them, they continue to be harassed in external spaces, such as personal websites, forums, public groups, etc. It gets even worse when marginalized people are more susceptible to harassment while also being emotionally burned out by having to constantly speak out about their situation, whether because of a disability, gender/sexuality, ethnicity, or other factors.

I would like to thank [Oro] (trans) and Ember (physical disability) for carefully reviewing this article. Oro has been [harassed](https://fosstodon.org/@TheEvilSkeleton/109587658504727925) by some communities because of their gender. As a friend, I think it's really important to raise awareness about protecting marginalized groups so that we can provide a safe space for them to express their identities.

---

**Footnotes**

[^1]: Bad faith in this context means to exclude groups by actively and intentionally harming them.

[Fresh Off the Boat]: https://en.wikipedia.org/wiki/Fresh_Off_the_Boat
[decorative images]: https://www.w3.org/WAI/tutorials/images/decorative
[homepage]: /
[Skip to main content]: https://accessibility.oit.ncsu.edu/it-accessibility-at-nc-state/developers/accessibility-handbook/mouse-and-keyboard-events/skip-to-main-content
[reduced motion]: https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-reduced-motion
[accessibility]: https://en.wikipedia.org/wiki/Accessibility
[movement disorders]: https://en.wikipedia.org/wiki/Movement_disorder
[OpenDyslexic]: https://opendyslexic.org
[Don't ask to ask, just ask]: https://dontasktoask.com
[free software movement]: https://en.wikipedia.org/wiki/Free_software_movement
[Politics in your FOSS project? It's more likely than you think!]: https://samsai.eu/post/politics-in-your-foss-project
[Oro]: https://orowith2os.gitlab.io
