---
layout: post
title: "How to Propose Features to GNOME"
description: "
Recently, GNOME added an option into GNOME Settings to adjust pointer acceleration, which was a feature that the developers and designers were originally against. One person managed to convince them, by giving *one* reason. Thanks to them, pointer acceleration options are now available in GNOME Settings!


Firstly, I'm going to summarize the relevant parts of the proposal and discussion behind the addition, and explain how it was accepted. Then, to build on top of that, GNOME's philosophy and the importance of taking it into consideration. And lastly, how to propose features to GNOME and what to avoid.


However, this article is not about whether GNOME is successful with their philosophy, and the tone of developers and designers. Additionally, this isn't about *where* to propose features, rather how to formulate the proposal and what to consider.
"
tags: ["GNOME", "Linux"]
toc: true
disclaimer: "I am not speaking on the behalf of GNOME."
redirect_from: /2023/03/24/how-to-propose-features-to-gnome.html
---

## Introduction

{{ page.description }}

## Summary of the Proposal and Discussion
Felipe Borges, a maintainer of GNOME Settings, submitted a [merge request](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1548) to make pointer acceleration options discoverable in GNOME Settings. However, [Georges Stavracas](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1548#note_1622617), a maintainer, and [Allan Day](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1548#note_1622772), a designer, were strongly against this addition and expressed that it does not benefit the majority of users and can lead to confusion. At that time, this feature was leaning towards rejection.

I [responded](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1548#note_1622680) that this feature can hugely benefit gamers, as many of them are sensitive to pointer acceleration during gameplay. I asked to reconsider this position, as there is a considerably large amount of people who would appreciate it (albeit still a minority).

Georges [responded](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1548#note_1622716) that they were still against it. They argued that it isn't relevant for most people, and the target audience will be left confused.[^1] In my opinion, ignoring the unjust rant, their reasoning was completely justified and valid. In the end, they were unconvinced.

Later, John Smith [commented](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1548#note_1631003) that having the flat profile (a nondefault option on GNOME) as opposed to the default is desirable for people who suffer from hand tremors (shaking/fidgety hands) -- John often needs to tweak pointer acceleration within GNOME Tweaks for their relative, who suffers from Parkinson. They also pointed out that they use GNOME Tweaks more often than GNOME Settings because of that feature.

Afterwards, the developers and designers were convinced, and then [discussed](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1548#note_1632298) with John Smith on adding it and wording it appropriately. The merge request was then accepted and merged, and finally added to GNOME Settings in GNOME 44.

## Importance of Taking GNOME's Philosophy Into Consideration
So what happened? How come I couldn't convince the developers and designers even though my statement was correct, while John Smith convinced them even though their statement was just as correct?

That is because John Smith took GNOME's philosophy (especially target audience) into consideration, whereas I didn't. While both of our points were correct, mine was, to put it in the nicest way possible, irrelevant. This sole difference between our approaches lead to opposite directions of this merge request, as only John Smith's approach contributed to GNOME's philosophy, and thus literally made the developers and designers reconsider.

Understanding GNOME's philosophy and taking it into consideration is, in my opinion, the most important factor when proposing *anything* to GNOME. GNOME takes the philosophy very seriously, so it is really important that proposals satisfy it. As shown above, this is a matter of acceptance and rejection.

## What is GNOME's Philosophy?
GNOME's philosophy is sophisticated and there is a lot of room for misunderstanding. Keep in mind that I wrote an article about GNOME's philosophy in "[What is GNOME's Philosophy?]({% post_url 2023-04-02-what-is-gnomes-philosophy %})", where it explains in depth.

To summarize the article, productivity is a key focus for GNOME. However, GNOME approaches it in a very specific manner: The target audiences are people with disabilities, the average computer and mobile user, as well as developers interested in GNOME. GNOME aims to create a coherent environment and experience, courtesy of the [Human Interface Guidelines], in which the guidelines are heavily based on cognitive ergonomics, such as perception, memory, reasoning, and motor response. GNOME also encourages peacefulness and organization by discouraging some (sometimes conventional) features.

This means, GNOME is fully open with feature proposals, as long as the feature complies with the philosophy.

## How Should I Propose Features?
Now that we (hopefully) have a better understanding of GNOME's philosophy and why it's important to take it into consideration, let's look at how I would recommend proposing features.

There are many important notes and recommendations I'd like to make whenever you propose anything (including features) to GNOME:
- Correctness and relevance are different from one another. Being correct doesn't necessarily mean that the proposal will contribute to GNOME's philosophy. Relevancy, however, ensures that the information satisfies with GNOME's philosophy, so it's important that proposals are relevant, as shown with the merge request.
- Using GNOME primarily doesn't automatically make you its target audience. Please take the time to ask yourself if your proposal contributes to GNOME's philosophy. If it does, explain how it benefits the *target audience*, not how it benefits you. Same goes to commenting/replying to someone.
- Follow the GNOME [Code of Conduct]. Being respectful to others is a really important step for a healthy relationship between you and GNOME. Of course, it doesn't mean that every party will behave respectfully, including GNOME members.
- This isn't 100% guaranteed, and may be really exhausting. Don't rush yourself to reply as soon as possible. If you are tired or aren't in the mood for replying, then comment another time. The last thing you'd want to do is offend a maintainer and have your proposal rejected, whether it is done intentionally or not.
- Unfortunately, some members may behave poorly (like in that merge request) and even violate the Code of Conduct without dire consequences. Dealing with unjust situations can be difficult, even if you were respectful. If a member is behaving inappropriately, you can try to follow [Procedure For Reporting Code of Conduct Incidents]. In any case, make sure not to seem offensive or demanding.

Whenever you propose a feature to GNOME, ask yourself this question: "**Does it comply with GNOME's Philosophy?**"

If it does, then GNOME developers and designers will likely be interested in your idea. Of course, like explained above, proper communication is crucial, as well as wording it appropriately. Nevertheless, if the proposal fundamentally goes against the philosophy, then it will very likely be rejected.

## Conclusion
One thing I've learned from experience is that GNOME mainly cares about proposals that serve the philosophy, as they take it very seriously.

Providing good user experience is difficult, as [preferences have a cost](https://ometer.com/preferences.html). It can also be difficult to know what features the target audience would want, especially when they are nontechnical users, as many of them may not know how and who to contact. Having people propose features is a wonderful privilege for all of us, so it's really important that we do it with care, and put the time and effort to word it in a way that we consider the philosophy, and explain thoroughly how it contributes to it.

Hopefully, this helps you understand GNOME's goals with the desktop and its philosophy. With a better understanding, you should be able to carefully propose and formulate the feature you want.

---

Edit 1: Add bullet point for members behaving inappropriately (credit to [Maksym Hazevych](https://mstdn.social/@mks_h))

---

**Footnotes**

[^1]: Mouse Acceleration is the operating system that artificially imposes acceleration. This means that even though the mouse movement physically moves at a constant speed from start to finish, the software makes the cursor move slowly at the beginning and work its way up until a certain threshold. The disparity between the hardware (mouse/touchpad) and software (cursor) is the source of the confusion.

[Cognitive ergonomics]: https://wikipedia.org/wiki/Cognitive_ergonomics
[information overload]: https://en.wikipedia.org/wiki/Information_overload
[Human Interface Guidelines]: https://developer.gnome.org/hig
[eye strain]: https://en.wikipedia.org/wiki/Eye_strain
[Code of Conduct]: https://wiki.gnome.org/Foundation/CodeOfConduct
[libadwaita]: https://gitlab.gnome.org/GNOME/libadwaita
[GNOME Circle]: https://circle.gnome.org
[Blueprint]: https://gitlab.gnome.org/jwestman/blueprint-compiler
[This Week in GNOME]: https://thisweek.gnome.org
[Google Summer of Code]: https://gsoc.gnome.org
[Outreachy]: https://wiki.gnome.org/Outreach/Outreachy
[Procedure For Reporting Code of Conduct Incidents]: https://wiki.gnome.org/Foundation/CodeOfConduct/ReporterGuide
*[FOSS]: Free and open source Software
*[responsiveness]: Applications that adapt on form factors

<!-- *[conditions]: Forgetting, confused, disoriented, etc. -->
