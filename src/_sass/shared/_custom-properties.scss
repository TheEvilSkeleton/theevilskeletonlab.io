/*
 * _custom-properties.scss
 * Copyright (C) 2024 Hari Rana / TheEvilSkeleton and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 */


@use "sass:string";

@function fallback($optional: "", $fallback) {
  @if string.length($optional) != 0 {
    @return $optional;
  }
  @else {
    @return $fallback;
  }
}

@mixin dark-color-scheme() {
  --accent-background-lightness: 28.66%;
  --body-foreground-lightness: 85%;
  --body-background-lightness: 24.56%;
  --header-background-lightness: 29%;
  --html-background-lightness: var(--header-background-lightness);
  --code-background-lightness: 32.7%;
  --codeblock-background-lightness: 16%;
  --blockquote-border-start-lightness: 52%;
  --heading-foreground-lightness: 92.25%;
  --header-select-lightness: 75.8%;
  --hyperlink-lightness: 76.27%;
  --hyperlink-hover-lightness: 81.19%;
  --hyperlink-focus-lightness: 58.73%;
  --text-select-background-lightness: 46.33%;
  --shadow-lightness: 20%;
  --target-background-lightness: 38%;
  --table-outline-lightness: 45%;
  --table-row-odd-lightness: 36%;
  --table-row-even-lightness: 30%;
  --scrollbar-foreground-lightness: var(--header-select-lightness);
  --scrollbar-background-lightness: var(--code-background-lightness);
  --title-background-chroma: 0.105;
  --codeblock-outline-lightness: 34.125%;
  --separator-lightness: var(--blockquote-border-start-lightness);
  --_high-contrast-foreground-lightness: 98.72%;

  --trans-gradient:
    linear-gradient(to bottom right, oklch(28.35% 0.012 258.37), oklch(var(--header-background-lightness) 0 0), oklch(26.82% 0.011 17.83));

  /*
   * Based on libadwaita:
   *  https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/named-colors.html
   */
  --orange: oklch(73% 0.17 28.31);
  --mint: oklch(76% 0.11 175.98);
  --light-orange: oklch(79% 0.15 62.25);
  --red: oklch(69% 0.13 0);
  --blue: oklch(69.56% 0.127 253.81);
  --green: oklch(71% 0.21 136.16);
}

@mixin light-color-scheme {
  --accent-background-lightness: 95.15%;
  --body-foreground-lightness: 28.99%;
  --body-background-lightness: 99.35%;
  --header-background-lightness: 96.75%;
  --html-background-lightness: var(--header-background-lightness);
  --code-background-lightness: 90.57%;
  --codeblock-background-lightness: 93.77%;
  --blockquote-border-start-lightness: 79.24%;
  --heading-foreground-lightness: 22.99%;
  --header-select-lightness: 55%;
  --hyperlink-lightness: 45%;
  --hyperlink-hover-lightness: 49.36%;
  --hyperlink-focus-lightness: 40%;
  --text-select-background-lightness: 66%;
  --shadow-lightness: 88%;
  --target-background-lightness: 88%;
  --table-outline-lightness: 90%;
  --table-row-odd-lightness: 94.61%;
  --table-row-even-lightness: 97.61%;
  --title-background-chroma: 0.2;
  --codeblock-outline-lightness: unset;
  --_high-contrast-foreground-lightness: var(--heading-foreground-lightness);

  --trans-gradient: linear-gradient(oklch(94.02% 0.01 219.59), oklch(var(--header-background-lightness) 0 0), oklch(94% 0.01 6.79));

  --orange: oklch(40% 0.12 40.08);
  --mint: oklch(41% 0.07 0);
  --light-orange: var(--orange);
  --red: oklch(45% 0.16 0);
  --blue: oklch(49.23% 0.151 256.36);
  --green: oklch(42% 0.12 135.12);
}

@mixin high-contrast($value: "") {
  --opacity: #{fallback($value, 1)};
  --high-contrast-lightness: #{fallback($value, var(--body-foreground-lightness))};
  --high-contrast-border-lightness: #{fallback($value, var(--header-select-lightness))};
  --high-contrast-header-select-color: #{fallback($value, transparent)};
  --heading-foreground-lightness: var(--_high-contrast-foreground-lightness);
}

@mixin reduced-motion($value: "") {
  --animation-speed: #{fallback($value, 0s)};
  --animation: #{fallback($value, 0)};
}

/* Global custom properties */
:root {
  --accent-hue: 296.9525485;
  --heading-margin-block-start: 1.65rem;
  --heading-margin-block-end: 0.35rem;
  --title-font-size: clamp(1.95rem, 5vw + 0.00125rem, 2.725rem);
  --font-size: clamp(1.0125rem, 2.25vw + 0.00125rem, 1.125rem);
  --font-size-larger: 1.25em;
  --font-size-smaller: 0.85em;
  --main-margin-inline: 42px;
  --max-width: 725px;
  --breakout-tiny-max-width: 850px;
  --breakout-small-max-width: 1150px;
  --breakout-large-max-width: 1450px;
  --content-width: calc(min(100% - var(--main-margin-inline), var(--max-width)));
  --breakout-tiny-width: minmax(0, calc((var(--breakout-tiny-max-width) - var(--max-width)) / 2));
  --breakout-small-width: clamp(var(--main-margin-inline) / 2, (100% - var(--max-width)) / 2, (var(--breakout-small-max-width) - var(--breakout-tiny-max-width)) / 2);
  --breakout-large-width: minmax(0, calc((var(--breakout-large-max-width) - var(--breakout-small-max-width)) / 2));
  --box-shadow-box: 0 0 6px 3px oklch(var(--shadow-lightness) 0 0);
  --box-drop-shadow: drop-shadow(0 0 3px oklch(var(--drop-shadow-lightness) 0 0));
  --high-contrast-foreground-color: oklch(var(--high-contrast-lightness) 0 0);
  --large-text-letter-spacing: -0.0125em;
  --drop-shadow-lightness: 15%;
  --main-line-height: 1.5;
  --body-margin: 0px;
  --archived-hue: 303.37;
  --trigger-hue: 29.235;
  --disclaimer-hue: 70.67;

  /* System styles */
  @include dark-color-scheme();

  @media (prefers-color-scheme: light) {
    @include light-color-scheme();
  }

  @media (prefers-contrast: more) {
    @include high-contrast();
  }

  @media (prefers-reduced-motion) {
    @include reduced-motion();
  }

  /* User-selected styles at runtime */
  &:has(#dark-color-scheme:checked) {
    color-scheme: dark;
    @include dark-color-scheme();
  }

  &:has(#light-color-scheme:checked) {
    color-scheme: light;
    @include light-color-scheme();
  }

  &:has(#high-contrast:checked) {
    @include high-contrast();
  }

  &:has(#low-contrast:checked) {
    @include high-contrast(unset);
  }

  &:has(#reduced-motion:checked) {
    @include reduced-motion();
  }

  &:has(#increased-motion:checked) {
    @include reduced-motion(unset);
  }

}