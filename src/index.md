---
title: TheEvilSkeleton
description: "Welcome to my personal website! My name is **Hari Rana** (pronounced as Harry). I’m a 23-year-old transgender developer and content writer from Montréal, Canada, and use any/all pronouns. I’m known as TheEvilSkeleton online, but you can call me Skelly, Tessy, or Tesk for short."
layout: general
redirect_from: /about.html
---

{% include pfp.html %}

{% include command.html command="whoami" %}

## About Me

{{ page.description }}

As a developer, I'm mainly involved in [GNOME](https://www.gnome.org/), and am a [GNOME Foundation member](https://foundation.gnome.org/membership/) since [2022](https://gitlab.gnome.org/Teams/MembershipCommittee/-/issues/308#note_1553263). I currently develop [GNOME Calendar](https://apps.gnome.org/Calendar/) as a volunteer and maintain [Upscaler](https://gitlab.gnome.org/World/Upscaler).

As a content writer, I primarily write articles around GNOME and GNOME-adjacent projects, and topics around marginalized groups. Most of these articles are meant for educational purposes, which usually take a few weeks to plan, research, author, and revise.

I enjoy watching anime and cartoons, but I am extremely picky about them. My favorite anime are [Baccano!](https://en.wikipedia.org/wiki/Baccano!) and [Durarara!!](https://en.wikipedia.org/wiki/Durarara!!), as these stories focus on various characters and different sides of the story. Both stories are written by [Ryōgo Narita](https://en.wikipedia.org/wiki/Ry%C5%8Dgo_Narita).

As for my favorite cartoons, they are [The Last Airbender](https://en.wikipedia.org/wiki/Avatar:_The_Last_Airbender) and [The Legend of Korra](https://en.wikipedia.org/wiki/The_Legend_of_Korra). These are my favorites for the same reason as Baccano! and Durarara!!, additionally the character development throughout the story.

My favorite movies are [Kung Fu Panda](https://en.wikipedia.org/wiki/Kung_Fu_Panda) and [How to Train Your Dragon](https://en.wikipedia.org/wiki/How_to_Train_Your_Dragon). Just like The Last Airbender and The Legend of Korra, there's a lot of character development in these stories.

My favorite video game is [Mindustry](https://mindustrygame.github.io/). It is a sandbox tower-defense game that is challenging and requires a lot of dedication.

My favorite song is [Shadow and Truth](https://music.youtube.com/watch?v=B_BRs_DTvqo) from [ONE Ⅲ NOTES](https://music.youtube.com/channel/UCiLnpMrMVih3Y_jd3tvoMsg), and my favorite artist is [auvic](https://soundcloud.com/auvicmusic).

## Contact

You can contact me on the following platforms:
- [\[matrix\]](https://matrix.org/): [@theevilskeleton:fedora.im](https://matrix.to/#/@theevilskeleton:fedora.im) (Preferred)
- Email: [Contact me](mailto:{{ "theevilskeleton@riseup.net" | encode_email }})<br>(No, I won't share invite links)

## Donate

Feel free to support my work by donating to me on one of the following platforms:
- [GitHub](https://github.com/sponsors/TheEvilSkeleton)
- [Ko-fi](https://ko-fi.com/theevilskeleton)
- [Liberapay](https://liberapay.com/TheEvilSkeleton)

## Socials and Contributions

You can ~~stalk~~ find me on the following platforms:
- Codeberg: [TheEvilSkeleton](https://codeberg.org/TheEvilSkeleton/)
- GitHub: [TheEvilSkeleton](https://github.com/TheEvilSkeleton)
- GitLab: [TheEvilSkeleton](https://gitlab.com/TheEvilSkeleton)
- GNOME GitLab: [TheEvilSkeleton](https://gitlab.gnome.org/TheEvilSkeleton)
- Mastodon: [@TheEvilSkeleton@treehouse.systems](https://social.treehouse.systems/@TheEvilSkeleton){:rel="external me"}

## Certifications

- [CS50x](https://courses.edx.org/certificates/3c726c5051fb4b9b84a0402770bff4d1) (Harvard University / edX)
- [CS50P](https://courses.edx.org/certificates/911f1d1fb34849af84b997cf58ec2856) (Harvard University / edX)
