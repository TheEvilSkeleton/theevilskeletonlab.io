---
title: Demo Page
description: "Demo page showcasing this website’s stylesheet"
layout: post
date: 2001-10-10
archived: "An archived section to indicate the post has been archived


[Hyperlink]()
"

trigger: "A content warning section


[Hyperlink]()
"
disclaimer: "A disclaimer section


[Hyperlink]()
"

presentation:
- title: "Row 1"
  image: "/assets/morphogenesis-d.webp"
  description: "Description"
- title: "Row 2"
  image: "/assets/morphogenesis-d.webp"
  description: "Description and image without drop shadow"
  drop_shadow: false
- title: "Row 3"
  image: "/assets/morphogenesis-d.webp"
  description: "Description with source link"
  source: "https://gitlab.gnome.org/GNOME/gnome-backgrounds/-/blob/main/backgrounds/morphogenesis-d.svg?ref_type=heads"
---

## Style

<fieldset markdown="1">
<legend>Color Scheme</legend>
<input type="radio" name="color-scheme" id="system-color-scheme" checked><label for="system-color-scheme">System</label><br>
<input type="radio" name="color-scheme" id="dark-color-scheme"><label for="dark-color-scheme">Dark</label><br>
<input type="radio" name="color-scheme" id="light-color-scheme"><label for="light-color-scheme">Light</label><br>
</fieldset>

<fieldset markdown="1">
<legend>Contrast</legend>
<input type="radio" name="contrast" id="system-contrast" checked><label for="system-contrast">System</label><br>
<input type="radio" name="contrast" id="high-contrast"><label for="high-contrast">High Contrast</label><br>
<input type="radio" name="contrast" id="low-contrast"><label for="low-contrast">Low Contrast</label><br>
</fieldset>

<fieldset markdown="1">
<legend>Motion</legend>
<input type="radio" name="motion" id="system-motion" checked><label for="system-motion">System</label><br>
<input type="radio" name="motion" id="reduced-motion"><label for="reduced-motion">Reduced Motion</label><br>
<input type="radio" name="motion" id="increased-motion"><label for="increased-motion">Increased Motion</label><br>
</fieldset>

## Typography

<fieldset markdown="1">
<legend>Headings</legend>
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6
</fieldset>

<fieldset markdown="1">
<legend>Text</legend>
- Normal text
- *Italic text*
- **Bold text**
- ~~Struck text~~
- `Code text`
- [Hyperlink]()
- [Internal link](/)
- [External link](https://example.com)
- <mark>Marked text</mark>
</fieldset>

<fieldset markdown="1">
<legend>Blockquote</legend>
> Blockquote
> 
> > Nested blockquote
</fieldset>

<fieldset markdown="1">
<legend>Lists</legend>
- Unordered list
- Unordered list
   - Nested unordered list
   - Nested unordered list
- Unordered list<br><br>

1. Ordered list
2. Ordered list
   1. Nested ordered list
   2. Nested ordered list
3. Ordered list<br><br>

- [x] Task list (checked)
- [ ] Task list (unchecked)
   - [x] Nested task list (checked)
   - [ ] Nested task list (unchecked)
</fieldset>

<fieldset markdown="1">
<legend>Inputs</legend>
<input type="checkbox" name="checkbox" id="checkbox1" checked><label for="checkbox1">Checkbox (default: checked)</label><br>
<input type="checkbox" name="checkbox" id="checkbox2"><label for="checkbox2">Checkbox (default: unchecked)</label><br>
<input type="checkbox" name="checkbox" id="checkbox3" disabled><label for="checkbox3">Checkbox (disabled)</label><br>

<input type="radio" name="radio" id="radio1" checked><label for="radio1">Radio (default: checked)</label><br>
<input type="radio" name="radio" id="radio2"><label for="radio2">Radio (default: unchecked)</label><br>
<input type="radio" name="radio" id="radio3" disabled><label for="radio3">Radio (disabled)</label><br>

<label for="text">Text:</label>
<input type="text" id="text" name="text"><br>

<label for="textarea">Textarea:</label>
<textarea id="textarea" name="textarea" type="textarea" placeholder="Enter your message here"></textarea>
</fieldset>

<!-- <fieldset markdown="1"> -->
<!-- <legend>Table</legend> -->
<!-- <table> -->
<!-- 	<caption>Table Caption</caption> -->
<!-- 	<thead> -->
<!-- 		<tr> -->
<!-- 			<th>Header 1</th> -->
<!-- 			<th>Header 2</th> -->
<!-- 			<th>Header 3</th> -->
<!-- 			<th>Header 4</th> -->
<!-- 			<th>Header 5</th> -->
<!-- 			<th>Header 6</th> -->
<!-- 			<th>Header 7</th> -->
<!-- 			<th>Header 8</th> -->
<!-- 			<th>Header 9</th> -->
<!-- 			<th>Header 10</th> -->
<!-- 		</tr> -->
<!-- 	</thead> -->
<!-- 	<tbody> -->
<!-- 		<tr> -->
<!-- 			<td>Row:1 Cell:1</td> -->
<!-- 			<td>Row:1 Cell:2</td> -->
<!-- 			<td>Row:1 Cell:3</td> -->
<!-- 			<td>Row:1 Cell:4</td> -->
<!-- 			<td>Row:1 Cell:5</td> -->
<!-- 			<td>Row:1 Cell:6</td> -->
<!-- 			<td>Row:1 Cell:7</td> -->
<!-- 			<td>Row:1 Cell:8</td> -->
<!-- 			<td>Row:1 Cell:9</td> -->
<!-- 			<td>Row:1 Cell:10</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>Row:2 Cell:1</td> -->
<!-- 			<td>Row:2 Cell:2</td> -->
<!-- 			<td>Row:2 Cell:3</td> -->
<!-- 			<td>Row:2 Cell:4</td> -->
<!-- 			<td>Row:2 Cell:5</td> -->
<!-- 			<td>Row:2 Cell:6</td> -->
<!-- 			<td>Row:2 Cell:7</td> -->
<!-- 			<td>Row:2 Cell:8</td> -->
<!-- 			<td>Row:2 Cell:9</td> -->
<!-- 			<td>Row:2 Cell:10</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>Row:3 Cell:1</td> -->
<!-- 			<td>Row:3 Cell:2</td> -->
<!-- 			<td>Row:3 Cell:3</td> -->
<!-- 			<td>Row:3 Cell:4</td> -->
<!-- 			<td>Row:3 Cell:5</td> -->
<!-- 			<td>Row:3 Cell:6</td> -->
<!-- 			<td>Row:3 Cell:7</td> -->
<!-- 			<td>Row:3 Cell:8</td> -->
<!-- 			<td>Row:3 Cell:9</td> -->
<!-- 			<td>Row:3 Cell:10</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>Row:4 Cell:1</td> -->
<!-- 			<td>Row:4 Cell:2</td> -->
<!-- 			<td>Row:4 Cell:3</td> -->
<!-- 			<td>Row:4 Cell:4</td> -->
<!-- 			<td>Row:4 Cell:5</td> -->
<!-- 			<td>Row:4 Cell:6</td> -->
<!-- 			<td>Row:4 Cell:7</td> -->
<!-- 			<td>Row:4 Cell:8</td> -->
<!-- 			<td>Row:4 Cell:9</td> -->
<!-- 			<td>Row:4 Cell:10</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>Row:5 Cell:1</td> -->
<!-- 			<td>Row:5 Cell:2</td> -->
<!-- 			<td>Row:5 Cell:3</td> -->
<!-- 			<td>Row:5 Cell:4</td> -->
<!-- 			<td>Row:5 Cell:5</td> -->
<!-- 			<td>Row:5 Cell:6</td> -->
<!-- 			<td>Row:5 Cell:7</td> -->
<!-- 			<td>Row:5 Cell:8</td> -->
<!-- 			<td>Row:5 Cell:9</td> -->
<!-- 			<td>Row:5 Cell:10</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>Row:6 Cell:1</td> -->
<!-- 			<td>Row:6 Cell:2</td> -->
<!-- 			<td>Row:6 Cell:3</td> -->
<!-- 			<td>Row:6 Cell:4</td> -->
<!-- 			<td>Row:6 Cell:5</td> -->
<!-- 			<td>Row:6 Cell:6</td> -->
<!-- 			<td>Row:6 Cell:7</td> -->
<!-- 			<td>Row:6 Cell:8</td> -->
<!-- 			<td>Row:6 Cell:9</td> -->
<!-- 			<td>Row:6 Cell:10</td> -->
<!-- 		</tr> -->
<!-- 	</tbody> -->
<!-- </table> -->
<!-- </fieldset> -->

## Breakout Elements

### Fieldset

<fieldset markdown="1">
<legend>Legend</legend>
Content
</fieldset>

### Preformatted Text

<div class="language-markdown highlighter-rouge"><div class="highlight"><pre class="highlight"><code>```LANGUAGE
INSERT TEXT
```
</code></pre></div></div>


```scss
body {
  min-height: 100dvh;
  color: oklch(var(--body-foreground-lightness) 0 0);
  background-color: oklch(var(--body-background-lightness) 0 0);
  font-family: system-ui, sans-serif;
  overflow-wrap: break-word;
  margin: 0;
  display: grid;
  grid-template-rows: auto 1fr auto;
  font-size: var(--font-size);

  grid-template-columns:
    [full-width-start] 1fr [breakout-large-start] var(--breakout-large-width) [breakout-small-start] var(--breakout-small-width) [breakout-tiny-start] var(--breakout-tiny-width) [content-start] var(--content-width) [content-end] var(--breakout-tiny-width) [breakout-tiny-end] var(--breakout-small-width) [breakout-small-end] var(--breakout-large-width) [breakout-large-end] 1fr [full-width-end];

  /* Header */
  & > header {
    --_padding-block: 12px;

    overflow: auto;
    background-color: oklch(var(--header-background-lightness) 0 0);
    box-shadow: var(--box-shadow-box);
    padding-block: var(--_padding-block);
    font-weight: 600;
    border-radius: var(--element-border-radius);
    outline: solid 1px var(--high-contrast-border-color);

    a {
      --_baseline-transition-properties: background-color, color, transform;

      border-radius: 6px;
      padding: 12px;
      text-decoration: none;
      color:
        var(--high-contrast-border-color,
        oklch(var(--header-select-lightness) 0.15 var(--accent-hue)));
      transition-duration: var(--animation-speed, 0.25s);
      transition-property: var(--_baseline-transition-properties);
      letter-spacing: var(--large-text-letter-spacing);

      &:hover {
        background-color:
          var(--high-contrast-header-select-color,
          oklch(var(--header-select-lightness) 0.15 var(--accent-hue)));
        color:
          var(--high-contrast-border-color,
          oklch(var(--header-background-lightness) 0 0));
        outline: dotted 2px var(--high-contrast-border-color);
      }

      &:focus {
        outline: solid 4px oklch(var(--scrollbar-foreground-lightness) 0.15 var(--accent-hue));
        transition-property: var(--_baseline-transition-properties), outline-width, outline-offset;
        outline-offset: 2px;
      }
    }

    .header-left, .skip-navigation {
      font-size: var(--font-size-larger);
      font-weight: 900;
      background-color: oklch(var(--header-background-lightness 0 0));
    }

    .skip-navigation {
      position: absolute;
      transform: translateY(-200%);
      margin-inline-start: 12px;

      &:focus {
        transform: translateY(0);
      }
    }

    nav {
      .header-left {
        display: none;
      }

      .header-main {
        display: flex;
        justify-content: space-evenly;

        a {
          text-align: center;

          &.active {
            background-color:
              var(--high-contrast-header-select-color,
              oklch(var(--header-select-lightness) 0.15 var(--accent-hue)));
            color:
              var(--high-contrast-border-color,
              oklch(var(--header-background-lightness) 0 0));
            outline: solid 2px var(--high-contrast-border-color);
          }
        }
      }
    }
  }
}
```

### Figures

#### Images

{% raw %}
```liquid
{%
  include image.html
  url="INSERT LINK"
  caption="INSERT CAPTION"
%}
```
{% endraw %}

{%
  include image.html 
  url="/assets/mindustry-demo.webp"
  caption="Screenshot from [Mindustry](https://mindustrygame.github.io/) with conveyor belts transporting resources into vaults."
%}

#### Videos

{% raw %}
```liquid
{%
  include video.html
  url="INSERT LINK"
  caption="INSERT CAPTION"
%}
```
{% endraw %}

{%
  include video.html
  url="/assets/mindustry-demo.mp4"
  caption="Screencast of [Mindustry](https://mindustrygame.github.io/) with me taking over control of tier 5 units and firing."
%}

### Presentation Table

This makes use of Jekyll's [Data Files](https://jekyllrb.com/docs/datafiles/) to generate the presentation table. Alternatively, the front matter can be used.

YAML syntax:
```yaml
presentation_name:
- title: "INSERT TITLE"
  image: "INSERT IMAGE SOURCE"
  description: "INSERT DESCRIPTION"
- title: "INSERT TITLE"
  image: "INSERT IMAGE SOURCE"
  description: "INSERT DESCRIPTION"
  drop_shadow: false # Optional for images that include a drop shadow
- title: "INSERT TITLE"
  image: "INSERT IMAGE SOURCE"
  description: "INSERT DESCRIPTION"
  source: "INSERT LINK" # Optional for source links
```

Liquid syntax:
{% raw %}
```liquid
{%
  include presentation.html
  rows="INSERT DATA"
%}
```
{% endraw %}

{% include presentation.html rows=page.presentation %}

## Automatic Color Adaptations

You can simply modify the `--accent-hue` custom property for it to change the accent color.

{% include demo-accent-elements.html hue="0" %}
{% include demo-accent-elements.html hue="52.5" %}
{% include demo-accent-elements.html hue="96.8" %}
{% include demo-accent-elements.html hue="155.45" %}
{% include demo-accent-elements.html hue="262.25" %}

