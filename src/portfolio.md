---
title: "Hari Rana’s Portfolio"
header_title: "Portfolio"
description: "My name is **Hari Rana**, a developer, technical writer, and content writer from Montréal, Canada. This is a list of free and open source projects I've worked on or am a part of. You will find some things I previously worked on for the free and open-source community here as well."
layout: general
order: 2

bottles_tags:
- Python
- GTK 4

upscaler_tags:
- Python
- GTK 4

refine_tags:
- Python
- GTK 4

gnome_calendar_tags:
- C
- GTK 4

gnome_control_center_tags:
- C
- GTK 4

theevilskeleton_gitlab_io_tags:
- Ruby
- CSS
- HTML

overlay_tags:
- Python
- argparse

finance_tags:
- Python
- JavaScript
- Flask
- Bootstrap

---

{% include command.html command="open portfolio.html" %}

{{ page.description }}

## Software Engineer • [Bottles]

{% include tag-list.html rows=page.bottles_tags %}

I help to develop and maintain critical components to establish a secure environment for running Windows software on Linux. It has gathered over **2,375,000 installations** over time, with an average of 3,250 installations per day, making it one of the **top 10** most downloaded applications on Flathub and one of the most popular applications on the Steam Deck.

{% include image.html
url="/assets/bottles.webp"
url-light="/assets/bottles-light.webp"
drop_shadow=false
lazy=false
%}

## Software Engineer • [Upscaler]

{% include tag-list.html rows=page.upscaler_tags %}

I build and maintain a graphical application for upscaling and enhancing images locally, which started out as my final project for *Harvard's Introduction to Computer Science* course. It has been downloaded over **125,000** times and is currently being utilized by **NASA**.

{% include image.html
url="/assets/Upscaler/embed.webp"
url-light="/assets/Upscaler/embed-light.webp"
drop_shadow=false
%}

## Software Engineer • [Calendar]

{% include tag-list.html rows=page.gnome_calendar_tags %}

I **collaborate** with professional software engineers and designers to build and maintain the **default pre-installed** calendar application on **millions** of Linux systems.

{% include image.html
url="/assets/gnome-calendar.webp"
url-light="/assets/gnome-calendar-light.webp"
drop_shadow=false
%}

## Accessibility, Software Engineer • [Settings]

{% include tag-list.html rows=page.gnome_control_center_tags %}

I **collaborate** and discuss with professional software engineers and designers to improve the **accessibility** and **responsiveness** of the core system application that is pre-installed on **millions** of Linux systems.

{% include image.html
url="/assets/gnome-control-center.webp"
url-light="/assets/gnome-control-center-light.webp"
drop_shadow=false
%}

## Software Engineer • [Refine]

{% include tag-list.html rows=page.refine_tags %}

An application I'm building and maintaining as a means of experimenting with ways to write code that is both **fast** and **reliable** while maintaining **scalability** and **ease of maintenance**. It leverages the fundamentals of **[data-driven]**, **object-oriented programming**, and **composition** paradigms.

{% include image.html
url="/assets/Refine/embed.webp"
url-light="/assets/Refine/embed-light.webp"
drop_shadow=false
%}

## Accessibility • [theevilskeleton.gitlab.io]

{% include tag-list.html rows=page.theevilskeleton_gitlab_io_tags %}

The personal [Jekyll]-based theme that powers this site with a strong focus on **responsiveness** and **accessibility**, allowing people with various disabilities and impairments or lower class economies to use, consume and navigate this site with ease.

## Software Engineer • [overlay]

{% include tag-list.html rows=page.overlay_tags %}

I developed this Python library while taking *Harvard's Introduction to Python* course. It is currently being utilized by Bottles, one of the most popular applications on the Steam Deck.

```
usage: project.py [-h] [--check-layers] {vkbasalt,mangohud} ...

parse and write configurations for vkBasalt and MangoHud

options:
  -h, --help           show this help message and exit
  --check-layers       check if vkBasalt, MangoHud and Mangoapp are available

layers:
  layer specific options

  {vkbasalt,mangohud}
    vkbasalt           use vkbasalt
    mangohud           use mangohud
```

## Course Project • [Finance]

{% include tag-list.html rows=page.finance_tags %}

I developed a website that enables users to purchase and trade virtual stocks using the IEX Cloud API for the *Harvard's Introduction to Computer Science* course. The website was built using Python and JavaScript with Flask and Bootstrap, allowing me to learn and experiment with various web and CSS frameworks.

<!-- {% include image.html url="/assets/finance.webp" %} -->

## Technical Writing, Technical Translator • [Flatseal]

I originally authored and continued improving Flatseal's documentation, ensuring effective communication of technical information to advanced users. Additionally, I translated the entire application into French, making it accessible for French speakers.

## Technical Writing • [Flatpak]

I regularly improve Flatpak's documentation, ensuring it is more intuitive for developers to ship their applications on Linux.

## Technical Writing • [Fedora Websites & Apps]

I originally wrote the documentation and contributing guidelines for the Fedora Websites & Apps team, which were designed to streamline the contribution process for both contributors and interns, facilitating the maintenance and development of the Fedora Project's website.

<!-- TODO: Rewrote these sections -->
<!-- ## [GNOME Foundation] -->

<!-- A nonprofit organization that funds and coordinates the GNOME desktop environment. -->

<!-- I'm a [member of the GNOME Foundation](https://foundation.gnome.org/membership), as I actively contribute to [GNOME software](https://gitlab.gnome.org/TheEvilSkeleton). I also write articles about GNOME on my blog. -->

<!-- {% include image.html url="/assets/gnome.webp" %} -->

<!-- ## [Vanilla OS] -->
<!-- A Linux distribution targeting the average computer and mobile users and developers. -->

<!-- I am a [member of Vanilla OS](https://vanillaos.org/team) and responsible for much of the user interface and experience. I contribute code whenever possible and moderate the official Discord server. -->

<!-- {% include image.html url="/assets/vanilla-os.webp" drop_shadow=false %} -->

<!-- ## [Consultant at Flathub] -->

<!-- Packaged and maintained a dozen of apps into [Flatpak](https://flatpak.org) and published them on [Flathub](https://flathub.org). -->

[free and open-source]: https://en.wikipedia.org/wiki/Free_and_open-source_software
[Bottles]: https://usebottles.com
[Upscaler]: /upscaler
[Refine]: /refine
[vkbasalt-cli]: https://gitlab.com/TheEvilSkeleton/vkbasalt-cli
[vkBasalt]: https://github.com/DadSchoorse/vkBasalt
[flatpak-dedup-checker]: https://gitlab.com/TheEvilSkeleton/flatpak-dedup-checker
[src_prepare]: https://src_prepare.gitlab.io
[vkbasalt-cli]: https://gitlab.com/TheEvilSkeleton/vkbasalt-cli
[overlay]: https://gitlab.com/TheEvilSkeleton/overlay
[GNOME Foundation]: https://foundation.gnome.org
[Vanilla OS]: https://vanillaos.org
[The Fedora Project]: https://fedoraproject.org
[Finance]: https://cs50.harvard.edu/x/2022/psets/9/finance
[IEX Cloud]: https://iexcloud.io/docs
[theevilskeleton.gitlab.io]: https://gitlab.com/TheEvilSkeleton/theevilskeleton.gitlab.io
[Jekyll]: https://jekyllrb.com
[Flatseal]: https://github.com/tchx84/Flatseal/pulls?q=is%3Apr+author%3ATheEvilSkeleton+
[Flatpak]: https://github.com/flatpak/flatpak-docs/pulls?q=is%3Apr+author%3ATheEvilSkeleton+
[Fedora Websites & Apps]: https://gitlab.com/groups/fedora/websites-apps/fedora-websites/-/wikis/Contributor-Guidelines
[Consultant at Flathub]: https://flathub.org/consultants
[Spell Checker]: https://cs50.harvard.edu/x/2022/psets/5/speller
[Image Recovery]: https://cs50.harvard.edu/x/2022/psets/4/recover
[Calendar]: https://apps.gnome.org/Calendar/
[data-driven]: https://en.wikipedia.org/wiki/Data-driven_programming
[Settings]: https://apps.gnome.org/Settings/
