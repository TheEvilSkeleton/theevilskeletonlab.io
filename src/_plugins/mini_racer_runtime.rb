# Copyright (c) 2015-2016 Sam Stephenson
# Copyright (c) 2015-2016 Josh Peek

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Upstream source:
#  https://github.com/rails/execjs/blob/master/lib/execjs/mini_racer_runtime.rb

require "execjs/runtime"

# Credits:
# - https://github.com/stevekinney/pizza/issues/103#issuecomment-136052789
# - https://github.com/stevekinney/pizza/pull/105

# TODO: Submit a merge request upstream with proper credits, and remove
# this file
Encoding.default_external = "UTF-8"

module ExecJS
  class MiniRacerRuntime < Runtime
    class Context < Runtime::Context
      def initialize(runtime, source = "", options={})
        source = source.encode(Encoding::UTF_8)
        @context = ::MiniRacer::Context.new
        @context.eval("delete this.console");
        translate do
          @context.eval(source)
        end
      end

      def exec(source, options = {})
        source = source.encode(Encoding::UTF_8)

        if /\S/ =~ source
          eval "(function(){#{source}})()"
        end
      end

      def eval(source, options = {})
        source = source.encode(Encoding::UTF_8)

        if /\S/ =~ source
          translate do
            @context.eval("(#{source})")
          end
        end
      end

      def call(identifier, *args)
        # TODO optimise generate
        eval "#{identifier}.apply(this, #{::JSON.generate(args)})"
      end

      private

      def strip_functions!(value)
        if Array === value
          value.map! do |v|
            if MiniRacer::JavaScriptFunction === value
              nil
            else
              strip_functions!(v)
            end
          end
        elsif Hash === value
          value.each do |k,v|
            if MiniRacer::JavaScriptFunction === v
              value.delete k
            else
              value[k] = strip_functions!(v)
            end
          end
          value
        elsif MiniRacer::JavaScriptFunction === value
          nil
        else
          value
        end
      end

      def translate
        begin
          strip_functions! yield
        rescue MiniRacer::RuntimeError => e
          ex = ProgramError.new e.message
          if backtrace = e.backtrace
            backtrace = backtrace.map { |line|
              if line =~ /JavaScript at/
                line.sub("JavaScript at ", "")
                    .sub("<anonymous>", "(execjs)")
                    .strip
              else
                line
              end
            }
            ex.set_backtrace backtrace
          end
          raise ex
        rescue MiniRacer::ParseError => e
          ex = RuntimeError.new e.message
          ex.set_backtrace(["(execjs):1"] + e.backtrace)
          raise ex
        end
      end

    end

    def name
      "mini_racer (V8)"
    end

    def available?
      require "mini_racer"
      true
    rescue LoadError
      false
    end
  end
end
