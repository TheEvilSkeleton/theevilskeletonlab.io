# MIT License

# Copyright (c) 2019 General UI LLC and its affiliates.
# Copyright (c) 2024 Hari Rana / TheEvilSkeleton and Contributors

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Sources:
# https://github.com/generalui/jekyll-image-size/blob/master/lib/jekyll-image-size.rb
# https://github.com/generalui/jekyll-image-size/pull/13

require "jekyll"
require 'fastimage'
require 'addressable/uri'

module Jekyll
  module MediaSizeToHtmlProps

    def size_to_html(input)
      uri = Addressable::URI.parse(input)

      if uri.scheme
        path = uri.to_s
      else
        path = File.join(@context.registers[:site].source, input)
      end

      # TODO: Add video support
      size = FastImage.size(path)

      width, height = size

      "width='#{width}' height='#{height}'"
    end
  end
end

Liquid::Template.register_filter(Jekyll::MediaSizeToHtmlProps)
