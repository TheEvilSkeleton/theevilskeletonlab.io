"""
Gemfile: Gem dependencies
Copyright (C) 2021 Hari Rana / TheEvilSkeleton and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>
"""

source "https://rubygems.org"

# Main source
gem "jekyll", "~> 4.3.3"

# Plugins sources
group :jekyll_plugins do
  gem "jekyll-feed"
  gem "jekyll-toc"
  gem "jekyll-seo-tag"
  gem "jekyll-email-protect"
  gem "jekyll-redirect-from"
  gem "jekyll-sitemap"
  gem "jekyll-gzip"
  gem "jekyll-minifier"
  gem "jekyll-autoprefixer-v2"
  gem "fastimage"
end

# Windows and JRuby does not include zoneinfo files, so bundle the tzinfo-data gem
# and associated library.
platforms :mingw, :x64_mingw, :mswin, :jruby do
  gem "tzinfo"
  gem "tzinfo-data"
end

# Performance-booster for watching directories on Windows
gem "wdm", :platforms => [:mingw, :x64_mingw, :mswin]

gem "webrick"
gem "csv"
gem "base64"
gem "bigdecimal"
gem "execjs"
gem "mini_racer"
gem "logger"
